# PyVtesMaker

Project to make Vampire: the Eternal Struggle cards. Codebase based upon awesome project https://gitlab.com/Thraen/pywowtcgmaker
WoW TCG was ancestor to entire branch of card games and got nice reborn set lately despite lack of commercial backing from nearly decade.
Many thanks to Self Biased for help in development, and Vtes community in general for support.


## License
For open source projects, say how it is licensed. I have no idea, most assets probably belong to Paradox, code is in in huge part from Thraen.

## Project status
In development. Or something.
