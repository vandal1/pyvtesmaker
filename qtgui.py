#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re
import uuid

from PySide6.QtWidgets import QApplication, QLineEdit, QPushButton, QCheckBox, QGridLayout, QWidget, QHBoxLayout, \
    QVBoxLayout, QTextEdit, QLabel, QComboBox, QScrollArea, QFileDialog, QColorDialog, QMessageBox, QRubberBand, QMenu
from PySide6.QtGui import QPixmap, QColor, QFont, QSyntaxHighlighter, QTextCharFormat, QAction, QMouseEvent, QTextCursor
import PySide6.QtCore as QtCore
from PIL.ImageQt import ImageQt, QImage
from PIL import Image
from math import sqrt

import json
import os
import shutil
import tempfile
import pathlib

from generator import WoWCardGenerator, Card

has_spellcheck = True
try:
    import enchant
    # print(enchant.list_languages())
    enchant.Dict("en_US")
except Exception:
    has_spellcheck = False

def dist(xa, ya, xb, yb):
    return sqrt((xb - xa) ** 2 + (yb - ya) ** 2)


class ImageButton(QLabel):
    toggle_signal = QtCore.Signal(str)

    def __init__(self, path, name, linked = False):
        super(ImageButton, self).__init__()
        self.name = name
        self.toggled = False
        self.linked = linked

        # Load pixmaps #
        pixmap = QPixmap()

        pixmap.load(path + name + " Inactive.png")
        self.inactive = pixmap.scaled(50, 50, QtCore.Qt.KeepAspectRatio)

        pixmap.load(path + name + " Hover.png")
        self.hover = pixmap.scaled(50, 50, QtCore.Qt.KeepAspectRatio)

        pixmap.load(path + name + ".png")
        self.active = pixmap.scaled(50, 50, QtCore.Qt.KeepAspectRatio)

        self.setPixmap(self.inactive)

    def activate(self):
        self.toggled = True
        self.setPixmap(self.active)

    def deactivate(self):
        self.toggled = False
        self.setPixmap(self.inactive)

    def mousePressEvent(self, _):
        if self.toggled and not self.linked:
            self.deactivate()
        else:
            self.activate()

        self.toggle_signal.emit(self.name)

    def enterEvent(self, _):
        if not (self.linked and self.toggled):
            self.setPixmap(self.hover)

    def leaveEvent(self, _):
        if self.toggled:
            self.setPixmap(self.active)
        else:
            self.setPixmap(self.inactive)


class CardArtHandler(QLabel):
    def __init__(self, wcg_gui):
        super(CardArtHandler, self).__init__()
        self.wcg_gui = wcg_gui
        self.rubberband = QRubberBand(QRubberBand.Rectangle, self)
        self.ax = 0
        self.ay = 0
        self.bx = 0
        self.by = 0
        self.max_x = 0
        self.max_y = 0
        self.dims = (0, 0)
        self.x_ratio = 0
        self.y_ratio = 0
        self.base_height = 0
        self.rubberband.show()

        self.state = "Idle"

        self.last_pos_x = None
        self.last_pos_y = None

    def mouseMoveEvent(self, event):
        if event.buttons() not in [QtCore.Qt.LeftButton, QtCore.Qt.RightButton]:
            return

        x = int(event.pos().x())
        y = int(event.pos().y())

        if event.buttons() == QtCore.Qt.LeftButton:
            if self.state == "Resize Top Left":
                if self.x_ratio > self.y_ratio:
                    self.ax = min(self.bx - 100, max(0, x))
                    self.ay = int(self.by - (self.bx - self.ax) * self.dims[1] / self.dims[0])
                    if self.ay < 0:
                        self.by -= self.ay
                        self.ay = 0
                else:
                    self.ay = min(self.by - 100, max(0, y))
                    self.ax = int(self.bx - (self.by - self.ay) * self.dims[0] / self.dims[1])
                    if self.ax < 0:
                        self.bx -= self.ax
                        self.ax = 0

            elif self.state == "Resize Bottom Left":
                if self.x_ratio > self.y_ratio:
                    self.ax = min(self.bx - 100, max(0, x))
                    self.by = int(self.ay + (self.bx - self.ax) * self.dims[1] / self.dims[0])
                    if self.by >= self.max_y:
                        self.ay -= self.by - self.max_y + 1
                        self.by = self.max_y - 1
                else:
                    self.by = max(self.ay + 100, min(self.max_y - 1, y))
                    self.ax = int(self.bx - (self.by - self.ay) * self.dims[0] / self.dims[1])
                    if self.ax < 0:
                        self.bx -= self.ax
                        self.ax = 0

            elif self.state == "Resize Top Right":
                if self.x_ratio > self.y_ratio:
                    self.bx = max(self.ax + 100, min(self.max_x - 1, x))
                    self.ay = int(self.by - (self.bx - self.ax) * self.dims[1] / self.dims[0])
                    if self.ay < 0:
                        self.by -= self.ay
                        self.ay = 0
                else:
                    self.ay = min(self.by - 100, max(0, y))
                    self.bx = int(self.ax + (self.by - self.ay) * self.dims[0] / self.dims[1])
                    if self.bx >= self.max_x:
                        self.ax -= self.bx - self.max_x + 1
                        self.bx = self.max_x - 1

            elif self.state == "Resize Bottom Right":
                if self.x_ratio > self.y_ratio:
                    self.bx = max(self.ax + 100, min(self.max_x - 1, x))
                    self.by = int(self.ay + (self.bx - self.ax) * self.dims[1] / self.dims[0])
                    if self.by >= self.max_y:
                        self.ay -= self.by - self.max_y + 1
                        self.by = self.max_y - 1
                else:
                    self.by = max(self.ay + 100, min(self.max_y - 1, y))
                    self.bx = int(self.ax + (self.by - self.ay) * self.dims[0] / self.dims[1])
                    if self.bx >= self.max_x:
                        self.ax -= self.bx - self.max_x + 1
                        self.bx = self.max_x - 1

        # Moving #
        else:
            x_diff = x - self.last_pos_x
            y_diff = y - self.last_pos_y

            if x_diff < 0 and self.ax > 0:
                x_diff = -min(self.ax, -x_diff)
            elif x_diff > 0 and self.bx < self.max_x - 1:
                x_diff = min(self.max_x - 1 - self.bx, x_diff)
            else:
                x_diff = 0
           
            if y_diff < 0 and self.ay > 0:
                y_diff = -min(self.ay, -y_diff)
            elif y_diff > 0 and self.by < self.max_y - 1:
                y_diff = min(self.max_y - 1 - self.by, y_diff)
            else:
                y_diff = 0

            self.ax += x_diff
            self.bx += x_diff
            self.ay += y_diff
            self.by += y_diff

            self.last_pos_x = x
            self.last_pos_y = y

        # Extra sanitize step #
        self.ax = max(0, self.ax)
        self.bx = min(self.max_x - 1, self.bx)
        self.ay = max(0, self.ay)
        self.by = min(self.max_y - 1, self.by)

        self.computeRubberband(True)
        
    def mousePressEvent(self, event):
        x = int(event.pos().x())
        y = int(event.pos().y())

        # Resizing #
        if event.buttons() == QtCore.Qt.LeftButton:    
            if abs(self.ax - x) < abs(x - self.bx):
                if abs(self.ay - y) < abs(y - self.by):
                    self.state = "Resize Top Left"
                else:
                    self.state = "Resize Bottom Left"
            else:
                if abs(self.ay - y) < abs(y - self.by):
                    self.state = "Resize Top Right"
                else:
                    self.state = "Resize Bottom Right"
        
        # Moving #
        if event.buttons() == QtCore.Qt.RightButton:
            self.last_pos_x = x
            self.last_pos_y = y

    def mouseReleaseEvent(self, event):
        ratio = self.base_height / self.max_y
        self.wcg_gui.card.art_box = (int(self.ax * ratio), int(self.ay * ratio), int(self.bx * ratio), int(self.by * ratio))
        self.wcg_gui.generateCard()
        self.state = "Idle"
        self.last_dist = 0

    def setArt(self, art, base_height, update=True):
        self.setPixmap(art)
        self.base_height = base_height
        self.max_x = art.rect().bottomRight().x()
        self.max_y = art.rect().bottomRight().y()

        if update:
            self.computeRubberband(new_art=True)
            self.mouseReleaseEvent(None)

    def computeRubberband(self, mouse_update=False, new_art=False):
        if self.wcg_gui.card.variant == "FullBleed" or "Alternate" in self.wcg_gui.card.variant:
            if self.dims != (692, 990):
                self.dims = (692, 990)
            elif not mouse_update and not new_art:
                return
        elif self.wcg_gui.card.variant == "Classic"  in self.wcg_gui.card.variant and self.wcg_gui.card.card_type == "Vampire" in self.wcg_gui.card.card_type:
            if self.dims != (542, 710):
                self.dims = (542, 710)
            elif not mouse_update and not new_art:
                return
        elif self.wcg_gui.card.variant == "Old"  in self.wcg_gui.card.variant and self.wcg_gui.card.card_type == "Vampire" in self.wcg_gui.card.card_type:
            if self.dims != (550, 710):
                self.dims = (550, 710)
            elif not mouse_update and not new_art:
                return
        elif self.wcg_gui.card.variant == "Classic" or "Old" in self.wcg_gui.card.variant and self.wcg_gui.card.card_type == "Minion" in self.wcg_gui.card.card_type:
            if self.dims != (572, 495):
                self.dims = (572, 495)
            elif not mouse_update and not new_art:
                return
        else:
            if self.dims != (692, 990):
                self.dims = (692, 990)
            elif not mouse_update and not new_art:
                return

        if not mouse_update:
            ratio = self.base_height / self.max_y
            # Get longer side taking card type into account #
            self.x_ratio =  self.dims[0] / self.max_x
            self.y_ratio = self.dims[1] / self.max_y
            if self.x_ratio > self.y_ratio:
                self.ax = 0
                self.bx = self.max_x
                height = int(self.max_x * self.dims[1] / self.dims[0])
                self.ay = int((self.max_y - height) / 2)
                self.by = self.ay + height
                self.rubberband.setGeometry(QtCore.QRect(self.ax, self.ay, self.max_x, height))

            else:
                self.ay = 0
                self.by = self.max_y
                width = int(self.max_y * self.dims[0] / self.dims[1])
                self.ax = int((self.max_x - width) / 2)
                self.bx = self.ax + width
                self.rubberband.setGeometry(QtCore.QRect(self.ax, self.ay, width, self.max_y))
                
            self.wcg_gui.card.art_box = (int(self.ax * ratio), int(self.ay * ratio), int(self.bx * ratio), int(self.by * ratio))

        else:
            self.rubberband.setGeometry(QtCore.QRect(self.ax, self.ay, self.bx - self.ax, self.by - self.ay))


class CardPreview(QLabel):

    def __init__(self, card):
        super(CardPreview, self).__init__()
        self.card = card
        self.preview_image = None
        self.selected_preview_image = None

    def mousePressEvent(self, _):
        self.toggle_signal.emit(self)


CardPreview.toggle_signal = QtCore.Signal(CardPreview)

class Highlighter(QSyntaxHighlighter):
    WORDS = r'(?iu)[\w\']+'

    def __init__(self, *args):
        QSyntaxHighlighter.__init__(self, *args)

        self.dict = None

    def setDict(self, dict):
        self.dict = dict

    def highlightBlock(self, text):
        if not self.dict:
            return

        format = QTextCharFormat()
        format.setUnderlineColor(QtCore.Qt.red)
        format.setUnderlineStyle(QTextCharFormat.SpellCheckUnderline)

        for word_object in re.finditer(self.WORDS, text):
            if not self.dict.check(word_object.group()):
                self.setFormat(word_object.start(),
                               word_object.end() - word_object.start(), format)


class SpellText(QTextEdit):
    def __init__(self, *args, **kwargs):
        QTextEdit.__init__(self, *args, **kwargs)

        if has_spellcheck:
            self.dict = enchant.Dict("en_US")
            self.highlighter = Highlighter(self.document())
            self.highlighter.setDict(self.dict)

    def mousePressEvent(self, event):
        if has_spellcheck:
            if event.button() == QtCore.Qt.RightButton:
                # Rewrite the mouse event to a left button event so the cursor is
                # moved to the location of the pointer.
                event = QMouseEvent(QtCore.QEvent.MouseButtonPress, event.pos(),
                                    QtCore.Qt.LeftButton, QtCore.Qt.LeftButton, QtCore.Qt.NoModifier)
            QTextEdit.mousePressEvent(self, event)

    def contextMenuEvent(self, event):
        if has_spellcheck:
            popup_menu = self.createStandardContextMenu()

            # Select the word under the cursor.
            cursor = self.textCursor()
            cursor.select(QTextCursor.WordUnderCursor)
            self.setTextCursor(cursor)

            # Check if the selected word is misspelled and offer spelling
            # suggestions if it is.
            if self.textCursor().hasSelection():
                text = self.textCursor().selectedText()
                if not self.dict.check(text):
                    spell_menu = QMenu('Spelling Suggestions')
                    for word in self.dict.suggest(text):
                        action = SpellAction(word, spell_menu)
                        action.correct.connect(self.correctWord)
                        spell_menu.addAction(action)
                    # Only add the spelling suggests to the menu if there are
                    # suggestions.
                    if len(spell_menu.actions()) != 0:
                        popup_menu.insertSeparator(popup_menu.actions()[0])
                        popup_menu.insertMenu(popup_menu.actions()[0], spell_menu)

            popup_menu.exec_(event.globalPos())

    def correctWord(self, word):
        '''
        Replaces the selected text with word.
        '''
        cursor = self.textCursor()
        cursor.beginEditBlock()

        cursor.removeSelectedText()
        cursor.insertText(word)

        cursor.endEditBlock()


class SpellAction(QAction):
    correct = QtCore.Signal(str)

    def __init__(self, *args):
        QAction.__init__(self, *args)

        self.triggered.connect(lambda x: self.correct.emit(
            self.text()))


class WoWCardGeneratorGUI(object):

    def __init__(self):
        # Instanciate the card generator #
        self.wcg = WoWCardGenerator()
        self.lock_generation = False
        self.card = Card()
        self.config = {}
        self.last_save_path = None
        self.bleeding = [752, 1050]

        if os.path.exists("config.json"):
            config_file = open("config.json", encoding="utf-8")
            self.config = json.load(config_file)
            config_file.close()

        if not "default_directory" in self.config:
            self.config["default_directory"] = ""

        # Create custom folder hierarchy #
        if not os.path.exists("Template/Custom"):
            os.mkdir("Template/Custom")
        if not os.path.exists("Template/Custom/Vampire"):
            os.mkdir("Template/Custom/Vampire")
        if not os.path.exists("Template/Custom/Classic Vampire"):
            os.mkdir("Template/Custom/Classic Vampire")
        if not os.path.exists("Template/Custom/Alternate Vampire"):
            os.mkdir("Template/Custom/Alternate Vampire")
        if not os.path.exists("Template/Custom/Old Vampire"):
            os.mkdir("Template/Custom/Old Vampire")
        if not os.path.exists("Template/Custom/Minion"):
            os.mkdir("Template/Custom/Minion")
        if not os.path.exists("Template/Custom/Classic Minion"):
            os.mkdir("Template/Custom/Classic Minion")
        if not os.path.exists("Template/Custom/Alternate Minion"):
            os.mkdir("Template/Custom/Alternate Minion")
        if not os.path.exists("Template/Custom/Old Minion"):
            os.mkdir("Template/Custom/Old Minion")
        if not os.path.exists("Template/Custom/Background"):
            os.mkdir("Template/Custom/Background")

        # Create the application #
        self.app = QApplication([])

        if not "font_size" in self.config:
            self.font_size = self.app.font().pointSize()

        else:
            self.font_size = self.config["font_size"]
            font = self.app.font()
            font.setPointSize(self.font_size)
            self.app.setFont(font)

        # Create the main window #
        self.main_frame = QWidget()
        self.main_frame.setWindowTitle("VtesCardGenerator")

        # Load default locale #
        locale_file = open("Locales/English.json", "r", encoding="utf-8")
        self.locale = json.load(locale_file)
        locale_file.close()

        # Instantiate the layouts #
        self.main_layout = QHBoxLayout()
        self.left_panel = QGridLayout()
        self.left_panel.setAlignment(QtCore.Qt.AlignTop)
        self.middle_panel = QGridLayout()
        self.middle_panel.setAlignment(QtCore.Qt.AlignTop)
        self.right_panel = QGridLayout()
        self.right_panel.setAlignment(QtCore.Qt.AlignTop)
        self.preview_panel = QVBoxLayout()
        self.preview_panel.setAlignment(QtCore.Qt.AlignTop)
        self.main_layout.addLayout(self.left_panel)
        self.main_layout.addLayout(self.middle_panel)
        self.main_layout.addLayout(self.right_panel)
        self.main_layout.addLayout(self.preview_panel)
        self.main_frame.setLayout(self.main_layout)

        self.left_rows = [0, 0]
        self.middle_rows = [0, 0]
        self.right_rows = [0]

        ## Left panel ##

        # Name #
        self.name_label = QLabel(self.locale["name"])
        self.name_text = QLineEdit()
        self.name_text.textChanged.connect(self.nameChanged)

        # Type #
        self.type_label = QLabel(self.locale["type"])
        self.type_combo_box = QComboBox()
        self.type_combo_box.addItems(self.locale["type_names"])
        self.type_combo_box.currentIndexChanged.connect(self.cardTypeChanged)

        self.type_names = self.locale["type_names"]

        # Variant #
        self.variant_label = QLabel(self.locale["variant"])
        self.variant_combo_box = QComboBox()
        self.variant_combo_box.currentIndexChanged.connect(self.variantChanged)
        self.variant_combo_box.setSizeAdjustPolicy(QComboBox.AdjustToContents)

        self.variant_names = self.locale["variant_names"]

        # Minion Card  Type #
        self.minion_type_label = QLabel(self.locale["minion_type"])
        self.minion_type_icons = QWidget()
        minion_type_icons_layout = QVBoxLayout()
        self.minion_type_icons.setLayout(minion_type_icons_layout)
        minion_type_row0_layout = QHBoxLayout()
        minion_type_row1_layout = QHBoxLayout()
        minion_type_row2_layout = QHBoxLayout()
        minion_type_row3_layout = QHBoxLayout()


        # Buttons dict #
        self.minion_type_button_dict = {}

        # Row 0#
        action_icon_button = ImageButton("Template/Editor/Minion/", "Action", True)
        action_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["Action"] = action_icon_button
        minion_type_row0_layout.addWidget(action_icon_button)

        political_icon_button = ImageButton("Template/Editor/Minion/", "Political", True)
        political_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["Political"] = political_icon_button
        minion_type_row0_layout.addWidget(political_icon_button)

        ally_icon_button = ImageButton("Template/Editor/Minion/", "Ally", True)
        ally_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["Ally"] = ally_icon_button
        minion_type_row0_layout.addWidget(ally_icon_button)

        retainer_icon_button = ImageButton("Template/Editor/Minion/", "Retainer", True)
        retainer_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["Retainer"] = retainer_icon_button
        minion_type_row0_layout.addWidget(retainer_icon_button)

        equipment_icon_button = ImageButton("Template/Editor/Minion/", "Equipment", True)
        equipment_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["Equipment"] = equipment_icon_button
        minion_type_row0_layout.addWidget(equipment_icon_button)

        modifier_icon_button = ImageButton("Template/Editor/Minion/", "Modifier", True)
        modifier_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["Modifier"] = modifier_icon_button
        minion_type_row0_layout.addWidget(modifier_icon_button)

        minion_type_icons_layout.addLayout(minion_type_row0_layout)

        # Row 1#
        reaction_icon_button = ImageButton("Template/Editor/Minion/", "Reaction", True)
        reaction_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["Reaction"] = reaction_icon_button
        minion_type_row1_layout.addWidget(reaction_icon_button)

        combat_icon_button = ImageButton("Template/Editor/Minion/", "Combat", True)
        combat_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["Combat"] = combat_icon_button
        minion_type_row1_layout.addWidget(combat_icon_button)

        action_combat_icon_button = ImageButton("Template/Editor/Minion/", "ActionCombat", True)
        action_combat_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["ActionCombat"] = action_combat_icon_button
        minion_type_row1_layout.addWidget(action_combat_icon_button)

        action_reaction_icon_button = ImageButton("Template/Editor/Minion/", "ActionReaction", True)
        action_reaction_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["ActionReaction"] = action_reaction_icon_button
        minion_type_row1_layout.addWidget(action_reaction_icon_button)

        modifier_combat_icon_button = ImageButton("Template/Editor/Minion/", "ModifierCombat", True)
        modifier_combat_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["ModifierCombat"] = modifier_combat_icon_button
        minion_type_row1_layout.addWidget(modifier_combat_icon_button)

        modifier_reaction_icon_button = ImageButton("Template/Editor/Minion/", "ModifierReaction", True)
        modifier_reaction_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["ModifierReaction"] = modifier_reaction_icon_button
        minion_type_row1_layout.addWidget(modifier_reaction_icon_button)

        minion_type_icons_layout.addLayout(minion_type_row1_layout)

        # Row 2#
        combat_reaction_icon_button = ImageButton("Template/Editor/Minion/", "CombatReaction", True)
        combat_reaction_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["CombatReaction"] = combat_reaction_icon_button
        minion_type_row2_layout.addWidget(combat_reaction_icon_button)

        combat_retainer_icon_button = ImageButton("Template/Editor/Minion/", "CombatRetainer", True)
        combat_retainer_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["CombatRetainer"] = combat_retainer_icon_button
        minion_type_row2_layout.addWidget(combat_retainer_icon_button)

        combat_equipment_icon_button = ImageButton("Template/Editor/Minion/", "CombatEquipment", True)
        combat_equipment_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["CombatEquipment"] = combat_equipment_icon_button
        minion_type_row2_layout.addWidget(combat_equipment_icon_button)

        modifier_ally_icon_button = ImageButton("Template/Editor/Minion/", "ModifierAlly", True)
        modifier_ally_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["ModifierAlly"] = modifier_ally_icon_button
        minion_type_row2_layout.addWidget(modifier_ally_icon_button)

        reaction_retainer_icon_button = ImageButton("Template/Editor/Minion/", "ReactionRetainer", True)
        reaction_retainer_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["ReactionRetainer"] = reaction_retainer_icon_button
        minion_type_row2_layout.addWidget(reaction_retainer_icon_button)

        event_icon_button = ImageButton("Template/Editor/Minion/", "Event", True)
        event_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["Event"] = event_icon_button
        minion_type_row2_layout.addWidget(event_icon_button)

        minion_type_icons_layout.addLayout(minion_type_row2_layout)

        master_icon_button = ImageButton("Template/Editor/Minion/", "Master", True)
        master_icon_button.toggle_signal.connect(self.minion_typeToggled)
        self.minion_type_button_dict["Master"] = master_icon_button
        minion_type_row3_layout.addWidget(master_icon_button)

        minion_type_icons_layout.addLayout(minion_type_row3_layout)

        self.current_minion_type_button = action_icon_button

        # Clan #
        self.clan_label = QLabel(self.locale["clan"])
        self.clan_icons = QWidget()
        clan_icons_layout = QVBoxLayout()
        self.clan_icons.setLayout(clan_icons_layout)
        clan_row0_layout = QHBoxLayout()
        clan_row1_layout = QHBoxLayout()
        clan_row2_layout = QHBoxLayout()

        # Buttons dict #
        self.clan_button_dict = {}

        # Row 0#
        banu_haquim_icon_button = ImageButton("Template/Editor/Clans/", "Banu Haquim", True)
        banu_haquim_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Banu Haquim"] = banu_haquim_icon_button
        clan_row0_layout.addWidget(banu_haquim_icon_button)

        brujah_icon_button = ImageButton("Template/Editor/Clans/", "Brujah", True)
        brujah_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Brujah"] = brujah_icon_button
        clan_row0_layout.addWidget(brujah_icon_button)

        gangrel_icon_button = ImageButton("Template/Editor/Clans/", "Gangrel", True)
        gangrel_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Gangrel"] = gangrel_icon_button
        clan_row0_layout.addWidget(gangrel_icon_button)

        hecata_icon_button = ImageButton("Template/Editor/Clans/", "Hecata", True)
        hecata_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Hecata"] = hecata_icon_button
        clan_row0_layout.addWidget(hecata_icon_button)

        lasombra_icon_button = ImageButton("Template/Editor/Clans/", "Lasombra", True)
        lasombra_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Lasombra"] = lasombra_icon_button
        clan_row0_layout.addWidget(lasombra_icon_button)

        malkavian_icon_button = ImageButton("Template/Editor/Clans/", "Malkavian", True)
        malkavian_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Malkavian"] = malkavian_icon_button
        clan_row0_layout.addWidget(malkavian_icon_button)

        clan_icons_layout.addLayout(clan_row0_layout)

        # Row 1#
        ministry_icon_button = ImageButton("Template/Editor/Clans/", "Ministry", True)
        ministry_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Ministry"] = ministry_icon_button
        clan_row1_layout.addWidget(ministry_icon_button)

        nosferatu_icon_button = ImageButton("Template/Editor/Clans/", "Nosferatu", True)
        nosferatu_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Nosferatu"] = nosferatu_icon_button
        clan_row1_layout.addWidget(nosferatu_icon_button)

        ravnos_icon_button = ImageButton("Template/Editor/Clans/", "Ravnos", True)
        ravnos_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Ravnos"] = ravnos_icon_button
        clan_row1_layout.addWidget(ravnos_icon_button)

        salubri_icon_button = ImageButton("Template/Editor/Clans/", "Salubri", True)
        salubri_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Salubri"] = salubri_icon_button
        clan_row1_layout.addWidget(salubri_icon_button)

        toreador_icon_button = ImageButton("Template/Editor/Clans/", "Toreador", True)
        toreador_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Toreador"] = toreador_icon_button
        clan_row1_layout.addWidget(toreador_icon_button)

        tremere_icon_button = ImageButton("Template/Editor/Clans/", "Tremere", True)
        tremere_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Tremere"] = tremere_icon_button
        clan_row1_layout.addWidget(tremere_icon_button)

        clan_icons_layout.addLayout(clan_row1_layout)

        # Row 2 #
        tzimisce_icon_button = ImageButton("Template/Editor/Clans/", "Tzimisce", True)
        tzimisce_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Tzimisce"] = tzimisce_icon_button
        clan_row2_layout.addWidget(tzimisce_icon_button)

        ventrue_icon_button = ImageButton("Template/Editor/Clans/", "Ventrue", True)
        ventrue_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["Ventrue"] = ventrue_icon_button
        clan_row2_layout.addWidget(ventrue_icon_button)

        none_icon_button = ImageButton("Template/Editor/Clans/", "None", True)
        none_icon_button.toggle_signal.connect(self.clanToggled)
        self.clan_button_dict["None"] = none_icon_button
        clan_row2_layout.addWidget(none_icon_button)

        clan_icons_layout.addLayout(clan_row2_layout)

        self.current_clan_button = banu_haquim_icon_button


        # Blood #
        self.blood_label = QLabel(self.locale["blood"])
        self.blood_text = QLineEdit()
        self.blood_text.textChanged.connect(self.bloodChanged)

        # Pool #
        self.pool_label = QLabel(self.locale["pool"])
        self.pool_text = QLineEdit()
        self.pool_text.textChanged.connect(self.poolChanged)

        # Capacity #
        self.capacity_label = QLabel(self.locale["capacity"])
        self.capacity_text = QLineEdit()
        self.capacity_text.textChanged.connect(self.capacityChanged)

        # Effect #
        self.effect_label = QLabel(self.locale["effect"])
        self.effect_text = SpellText()
        self.effect_text.textChanged.connect(self.effectChanged)



        # Background #
        self.background_label = QLabel(self.locale["background"])
        self.background_combo_box = QComboBox()
        self.background_combo_box.addItem("None")
        self.background_combo_box.addItems(os.listdir("Template/Custom/Background"))
        self.background_combo_box.currentIndexChanged.connect(self.backgroundChanged)

        ## Middle panel ##

        # Discipline icons #

        # Layouts #
        self.discipline_icons = QWidget()
        discipline_icons_layout = QVBoxLayout()
        self.discipline_icons.setLayout(discipline_icons_layout)
        discipline_row0_layout = QHBoxLayout()
        discipline_row1_layout = QHBoxLayout()
        discipline_row2_layout = QHBoxLayout()
        discipline_row3_layout = QHBoxLayout()
        discipline_row4_layout = QHBoxLayout()
        discipline_row5_layout = QHBoxLayout()
        discipline_row6_layout = QHBoxLayout()
        discipline_row7_layout = QHBoxLayout()
        discipline_row8_layout = QHBoxLayout()
        discipline_row9_layout = QHBoxLayout()

        # Discipline Dict #
        self.discipline_button_dict = {}

        # Label #
        self.discipline_label = QLabel(self.locale["disciplines"])
        discipline_icons_layout.addWidget(self.discipline_label)
        self.discipline_label.setAlignment(QtCore.Qt.AlignHCenter)

        # Row 0 #
        inferior_animalism_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Animalism")
        inferior_animalism_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Animalism"] = inferior_animalism_icon_button
        discipline_row0_layout.addWidget(inferior_animalism_icon_button)

        superior_animalism_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Animalism")
        superior_animalism_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Animalism"] = superior_animalism_icon_button
        discipline_row0_layout.addWidget(superior_animalism_icon_button)

        inferior_auspex_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Auspex")
        inferior_auspex_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Auspex"] = inferior_auspex_icon_button
        discipline_row0_layout.addWidget(inferior_auspex_icon_button)

        superior_auspex_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Auspex")
        superior_auspex_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Auspex"] = superior_auspex_icon_button
        discipline_row0_layout.addWidget(superior_auspex_icon_button)

        inferior_blood_sorcery_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Blood Sorcery")
        inferior_blood_sorcery_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Blood Sorcery"] = inferior_blood_sorcery_icon_button
        discipline_row0_layout.addWidget(inferior_blood_sorcery_icon_button)

        superior_blood_sorcery_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Blood Sorcery")
        superior_blood_sorcery_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Blood Sorcery"] = superior_blood_sorcery_icon_button
        discipline_row0_layout.addWidget(superior_blood_sorcery_icon_button)

        discipline_icons_layout.addLayout(discipline_row0_layout)

        # Row 1 #
        inferior_celerity_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Celerity")
        inferior_celerity_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Celerity"] = inferior_celerity_icon_button
        discipline_row1_layout.addWidget(inferior_celerity_icon_button)

        superior_celerity_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Celerity")
        superior_celerity_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Celerity"] = superior_celerity_icon_button
        discipline_row1_layout.addWidget(superior_celerity_icon_button)

        inferior_dominate_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Dominate")
        inferior_dominate_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Dominate"] = inferior_dominate_icon_button
        discipline_row1_layout.addWidget(inferior_dominate_icon_button)

        superior_dominate_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Dominate")
        superior_dominate_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Dominate"] = superior_dominate_icon_button
        discipline_row1_layout.addWidget(superior_dominate_icon_button)

        inferior_fortitude_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Fortitude")
        inferior_fortitude_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Fortitude"] = inferior_fortitude_icon_button
        discipline_row1_layout.addWidget(inferior_fortitude_icon_button)

        superior_fortitude_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Fortitude")
        superior_fortitude_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Fortitude"] = superior_fortitude_icon_button
        discipline_row1_layout.addWidget(superior_fortitude_icon_button)

        discipline_icons_layout.addLayout(discipline_row1_layout)

        # Row 2 #
        inferior_obfuscate_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Obfuscate")
        inferior_obfuscate_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Obfuscate"] = inferior_obfuscate_icon_button
        discipline_row2_layout.addWidget(inferior_obfuscate_icon_button)

        superior_obfuscate_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Obfuscate")
        superior_obfuscate_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Obfuscate"] = superior_obfuscate_icon_button
        discipline_row2_layout.addWidget(superior_obfuscate_icon_button)

        inferior_oblivion_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Oblivion")
        inferior_oblivion_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Oblivion"] = inferior_oblivion_icon_button
        discipline_row2_layout.addWidget(inferior_oblivion_icon_button)

        superior_oblivion_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Oblivion")
        superior_oblivion_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Oblivion"] = superior_oblivion_icon_button
        discipline_row2_layout.addWidget(superior_oblivion_icon_button)

        inferior_potence_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Potence")
        inferior_potence_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Potence"] = inferior_potence_icon_button
        discipline_row2_layout.addWidget(inferior_potence_icon_button)

        superior_potence_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Potence")
        superior_potence_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Potence"] = superior_potence_icon_button
        discipline_row2_layout.addWidget(superior_potence_icon_button)

        discipline_icons_layout.addLayout(discipline_row2_layout)

        # Row 3 #
        inferior_presence_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Presence")
        inferior_presence_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Presence"] = inferior_presence_icon_button
        discipline_row3_layout.addWidget(inferior_presence_icon_button)

        superior_presence_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Presence")
        superior_presence_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Presence"] = superior_presence_icon_button
        discipline_row3_layout.addWidget(superior_presence_icon_button)

        inferior_protean_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Protean")
        inferior_protean_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Protean"] = inferior_protean_icon_button
        discipline_row3_layout.addWidget(inferior_protean_icon_button)

        superior_protean_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Protean")
        superior_protean_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Protean"] = superior_protean_icon_button
        discipline_row3_layout.addWidget(superior_protean_icon_button)

        inferior_chimerstry_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Chimerstry")
        inferior_chimerstry_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Chimerstry"] = inferior_chimerstry_icon_button
        discipline_row3_layout.addWidget(inferior_chimerstry_icon_button)

        superior_chimerstry_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Chimerstry")
        superior_chimerstry_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Chimerstry"] = superior_chimerstry_icon_button
        discipline_row3_layout.addWidget(superior_chimerstry_icon_button)

        discipline_icons_layout.addLayout(discipline_row3_layout)

        inferior_dementation_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Dementation")
        inferior_dementation_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Dementation"] = inferior_dementation_icon_button
        discipline_row4_layout.addWidget(inferior_dementation_icon_button)

        superior_dementation_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Dementation")
        superior_dementation_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Dementation"] = superior_dementation_icon_button
        discipline_row4_layout.addWidget(superior_dementation_icon_button)

        inferior_necromancy_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Necromancy")
        inferior_necromancy_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Necromancy"] = inferior_necromancy_icon_button
        discipline_row4_layout.addWidget(inferior_necromancy_icon_button)

        superior_necromancy_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Necromancy")
        superior_necromancy_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Necromancy"] = superior_necromancy_icon_button
        discipline_row4_layout.addWidget(superior_necromancy_icon_button)

        inferior_obtenebration_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Obtenebration")
        inferior_obtenebration_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Obtenebration"] = inferior_obtenebration_icon_button
        discipline_row4_layout.addWidget(inferior_obtenebration_icon_button)

        superior_obtenebration_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Obtenebration")
        superior_obtenebration_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Obtenebration"] = superior_obtenebration_icon_button
        discipline_row4_layout.addWidget(superior_obtenebration_icon_button)

        discipline_icons_layout.addLayout(discipline_row4_layout)

        inferior_quietus_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Quietus")
        inferior_quietus_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Quietus"] = inferior_quietus_icon_button
        discipline_row5_layout.addWidget(inferior_quietus_icon_button)

        superior_quietus_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Quietus")
        superior_quietus_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Quietus"] = superior_quietus_icon_button
        discipline_row5_layout.addWidget(superior_quietus_icon_button)

        inferior_serpentis_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Serpentis")
        inferior_serpentis_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Serpentis"] = inferior_serpentis_icon_button
        discipline_row5_layout.addWidget(inferior_serpentis_icon_button)

        superior_serpentis_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Serpentis")
        superior_serpentis_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Serpentis"] = superior_serpentis_icon_button
        discipline_row5_layout.addWidget(superior_serpentis_icon_button)

        inferior_vicissitude_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Vicissitude")
        inferior_vicissitude_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Vicissitude"] = inferior_vicissitude_icon_button
        discipline_row5_layout.addWidget(inferior_vicissitude_icon_button)

        superior_vicissitude_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Vicissitude")
        superior_vicissitude_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Vicissitude"] = superior_vicissitude_icon_button
        discipline_row5_layout.addWidget(superior_vicissitude_icon_button)

        discipline_icons_layout.addLayout(discipline_row5_layout)

        inferior_daimonion_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Daimonion")
        inferior_daimonion_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Daimonion"] = inferior_daimonion_icon_button
        discipline_row6_layout.addWidget(inferior_daimonion_icon_button)

        superior_daimonion_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Daimonion")
        superior_daimonion_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Daimonion"] = superior_daimonion_icon_button
        discipline_row6_layout.addWidget(superior_daimonion_icon_button)

        inferior_melopomine_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Melopomine")
        inferior_melopomine_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Melopomine"] = inferior_melopomine_icon_button
        discipline_row6_layout.addWidget(inferior_melopomine_icon_button)

        superior_melopomine_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Melopomine")
        superior_melopomine_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Melopomine"] = superior_melopomine_icon_button
        discipline_row6_layout.addWidget(superior_melopomine_icon_button)

        inferior_mytherceria_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Mytherceria")
        inferior_mytherceria_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Mytherceria"] = inferior_mytherceria_icon_button
        discipline_row6_layout.addWidget(inferior_mytherceria_icon_button)

        superior_mytherceria_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Mytherceria")
        superior_mytherceria_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Mytherceria"] = superior_mytherceria_icon_button
        discipline_row6_layout.addWidget(superior_mytherceria_icon_button)

        discipline_icons_layout.addLayout(discipline_row6_layout)

        inferior_obeah_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Obeah")
        inferior_obeah_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Obeah"] = inferior_obeah_icon_button
        discipline_row7_layout.addWidget(inferior_obeah_icon_button)

        superior_obeah_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Obeah")
        superior_obeah_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Obeah"] = superior_obeah_icon_button
        discipline_row7_layout.addWidget(superior_obeah_icon_button)

        inferior_sanguinus_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Sanguinus")
        inferior_sanguinus_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Sanguinus"] = inferior_sanguinus_icon_button
        discipline_row7_layout.addWidget(inferior_sanguinus_icon_button)

        superior_sanguinus_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Sanguinus")
        superior_sanguinus_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Sanguinus"] = superior_sanguinus_icon_button
        discipline_row7_layout.addWidget(superior_sanguinus_icon_button)

        inferior_spiritus_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Spiritus")
        inferior_spiritus_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Spiritus"] = inferior_spiritus_icon_button
        discipline_row7_layout.addWidget(inferior_spiritus_icon_button)

        superior_spiritus_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Spiritus")
        superior_spiritus_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Spiritus"] = superior_spiritus_icon_button
        discipline_row7_layout.addWidget(superior_spiritus_icon_button)

        discipline_icons_layout.addLayout(discipline_row7_layout)

        inferior_temporis_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Temporis")
        inferior_temporis_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Temporis"] = inferior_temporis_icon_button
        discipline_row8_layout.addWidget(inferior_temporis_icon_button)

        superior_temporis_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Temporis")
        superior_temporis_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Temporis"] = superior_temporis_icon_button
        discipline_row8_layout.addWidget(superior_temporis_icon_button)

        inferior_thanatosis_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Thanatosis")
        inferior_thanatosis_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Thanatosis"] = inferior_thanatosis_icon_button
        discipline_row8_layout.addWidget(inferior_thanatosis_icon_button)

        superior_thanatosis_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Thanatosis")
        superior_thanatosis_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Thanatosis"] = superior_thanatosis_icon_button
        discipline_row8_layout.addWidget(superior_thanatosis_icon_button)

        inferior_valeren_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Valeren")
        inferior_valeren_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Valeren"] = inferior_valeren_icon_button
        discipline_row8_layout.addWidget(inferior_valeren_icon_button)

        superior_valeren_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Valeren")
        superior_valeren_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Valeren"] = superior_valeren_icon_button
        discipline_row8_layout.addWidget(superior_valeren_icon_button)

        discipline_icons_layout.addLayout(discipline_row8_layout)

        inferior_visceratika_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Visceratika")
        inferior_visceratika_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Visceratika"] = inferior_visceratika_icon_button
        discipline_row9_layout.addWidget(inferior_visceratika_icon_button)

        superior_visceratika_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Visceratika")
        superior_visceratika_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Visceratika"] = superior_visceratika_icon_button
        discipline_row9_layout.addWidget(superior_visceratika_icon_button)

        inferior_thaumaturgy_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Inferior Thaumaturgy")
        inferior_thaumaturgy_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Inferior Thaumaturgy"] = inferior_thaumaturgy_icon_button
        discipline_row9_layout.addWidget(inferior_thaumaturgy_icon_button)

        superior_thaumaturgy_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Superior Thaumaturgy")
        superior_thaumaturgy_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Thaumaturgy"] = superior_thaumaturgy_icon_button
        discipline_row9_layout.addWidget(superior_thaumaturgy_icon_button)

        flight_icon_button = ImageButton("Template/Editor/Discipline Icons/", "Flight")
        flight_icon_button.toggle_signal.connect(self.disciplineToggled)
        self.discipline_button_dict["Superior Flight"] = flight_icon_button
        discipline_row9_layout.addWidget(flight_icon_button)

        discipline_icons_layout.addLayout(discipline_row9_layout)

        # Advanced #
        self.advanced_label = QLabel(self.locale["advanced"])
        self.advanced_checkbox = QCheckBox()
        self.advanced_checkbox.clicked.connect(self.advancedChanged)

        # Block number #
        self.block_number_label = QLabel(self.locale["block_number"])
        self.block_number_text = QLineEdit()
        self.block_number_text.textChanged.connect(self.blockNumberChanged)

        # Set name #
        self.set_name_label = QLabel(self.locale["set_name"])
        self.set_name_text = QLineEdit()
        self.set_name_text.textChanged.connect(self.setNameChanged)

        # Set number widget #
        self.set_number_label = QLabel(self.locale["set_number"])
        self.set_number_widget = QWidget()
        set_number_layout = QHBoxLayout()
        self.set_number_widget.setLayout(set_number_layout)

        self.set_number_text = QLineEdit()
        self.set_number_text.textChanged.connect(self.setNumberChanged)
        set_number_layout.addWidget(self.set_number_text)

        set_number_separator = QLabel()
        set_number_separator.setText("/")
        set_number_layout.addWidget(set_number_separator)

        self.set_max_number_text = QLineEdit()
        self.set_max_number_text.textChanged.connect(self.setMaxNumberChanged)
        set_number_layout.addWidget(self.set_max_number_text)

        # Flavour #
        self.flavour_label = QLabel(self.locale["flavour"])
        self.flavour_text = SpellText()
        self.flavour_text.textChanged.connect(self.flavourChanged)

        ## Right panel ##

        # Image buttons layout #
        self.image_buttons = QWidget()
        image_buttons_layout = QHBoxLayout()

        # Import art button #
        self.import_art_button = QPushButton(self.locale["import_art"])
        self.import_art_button.clicked.connect(self.selectArt)
        image_buttons_layout.addWidget(self.import_art_button)

        # Resize art button #
        self.resize_art_button = QPushButton(self.locale["resize_art"])
        self.resize_art_button.clicked.connect(self.resizeArt)
        image_buttons_layout.addWidget(self.resize_art_button)

        # Save card button #
        self.save_card_button = QPushButton(self.locale["save_card"])
        self.save_card_button.clicked.connect(self.saveCard)
        image_buttons_layout.addWidget(self.save_card_button)

        self.image_buttons.setLayout(image_buttons_layout)

        # Save all cards button #
        self.save_all_cards_button = QPushButton(self.locale["save_all_cards"])
        self.save_all_cards_button.clicked.connect(self.saveAllCards)
        image_buttons_layout.addWidget(self.save_all_cards_button)

        self.image_buttons.setLayout(image_buttons_layout)

        # Save to printable button #
        self.save_to_printable_button = QPushButton(self.locale["save_to_printable"])
        self.save_to_printable_button.clicked.connect(self.saveToPrintable)
        image_buttons_layout.addWidget(self.save_to_printable_button)

        # Artist widget #
        self.artist_widget = QWidget()
        artist_layout = QHBoxLayout()
        self.artist_widget.setLayout(artist_layout)

        self.artist_label = QLabel(self.locale["artist"])
        artist_layout.addWidget(self.artist_label)

        self.artist_text = QLineEdit()
        self.artist_text.textChanged.connect(self.artistChanged)
        artist_layout.addWidget(self.artist_text)

        # Copyright widget #
        self.copyright_widget = QWidget()
        copyright_layout = QHBoxLayout()
        self.copyright_widget.setLayout(copyright_layout)

        self.copyright_label = QLabel(self.locale["copyright"])
        copyright_layout.addWidget(self.copyright_label)

        self.copyright_text = QLineEdit()
        self.copyright_text.textChanged.connect(self.copyrightChanged)
        copyright_layout.addWidget(self.copyright_text)

        # Border widget #
        self.border_widget = QWidget()
        border_layout = QHBoxLayout()
        self.border_widget.setLayout(border_layout)

        self.border_label = QLabel(self.locale["border"])
        border_layout.addWidget(self.border_label)

        self.border_color_button = QPushButton()
        self.border_color_button.clicked.connect(self.borderClicked)
        self.border_color_button.setStyleSheet("QPushButton{background-color : #000000;}")
        border_layout.addWidget(self.border_color_button)

        self.border_sharp_label = QLabel(self.locale["sharp"])
        border_layout.addWidget(self.border_sharp_label)

        self.border_sharp_checkbox = QCheckBox()
        self.border_sharp_checkbox.clicked.connect(self.borderSharpChanged)
        border_layout.addWidget(self.border_sharp_checkbox)

        self.bleeding_label = QLabel(self.locale["bleeding"])
        border_layout.addWidget(self.bleeding_label)

        self.bleeding_x_text = QLineEdit("720")
        self.bleeding_x_text.textChanged.connect(self.bleedingXChanged)
        border_layout.addWidget(self.bleeding_x_text)

        self.bleeding_y_text = QLineEdit("1050")
        self.bleeding_y_text.textChanged.connect(self.bleedingYChanged)
        border_layout.addWidget(self.bleeding_y_text)

        # JSON related buttons #
        self.json_load_button = QPushButton(self.locale["load_json"])
        self.json_load_button.clicked.connect(self.loadFromJSON)

        self.json_save_button = QPushButton(self.locale["save_json"])
        self.json_save_button.clicked.connect(self.saveToJSON)

        self.json_save_as_button = QPushButton(self.locale["save_json_as"])
        self.json_save_as_button.clicked.connect(self.saveToJSONAs)

        self.json_import_button = QPushButton(self.locale["import_json"])
        self.json_import_button.clicked.connect(self.importFromJSON)


        # Language / Font / Help widget #
        self.language_font_help_widget = QWidget()
        language_font_help_layout = QHBoxLayout()
        self.language_font_help_widget.setLayout(language_font_help_layout)

        self.language_label = QLabel(self.locale["language"])
        language_font_help_layout.addWidget(self.language_label)

        language_list = []
        for language_file in os.listdir("Locales"):
            language_list.append(language_file.split(".")[0])

        self.language_combo_box = QComboBox()
        self.language_combo_box.addItems(language_list)
        self.language_combo_box.setCurrentIndex(language_list.index("English"))
        self.language_combo_box.currentIndexChanged.connect(self.languageChanged)
        language_font_help_layout.addWidget(self.language_combo_box)

        self.font_label = QLabel(self.locale["font"])
        language_font_help_layout.addWidget(self.font_label)

        self.font_minus_button = QPushButton("-")
        self.font_minus_button.clicked.connect(self.fontMinusButtonClicked)
        language_font_help_layout.addWidget(self.font_minus_button)

        self.font_plus_button = QPushButton("+")
        self.font_plus_button.clicked.connect(self.fontPlusButtonClicked)
        language_font_help_layout.addWidget(self.font_plus_button)

        self.help_button = QPushButton("Help")
        self.help_button.clicked.connect(self.helpClicked)
        language_font_help_layout.addWidget(self.help_button)

        self.directory_button = QPushButton("Default directory")
        self.directory_button.clicked.connect(self.defaultDirectoryClicked)
        language_font_help_layout.addWidget(self.directory_button)

        # Non manual switch bool #
        self.nonManualLanguageSwitch = False

        # Card image #
        self.card_image = QLabel()

        ## Preview Panel ##
        self.preview_array = []
        self.preview_layout = QGridLayout()
        self.preview_layout.setAlignment(QtCore.Qt.AlignTop | QtCore.Qt.AlignLeft)
        preview_widget = QWidget()
        preview_widget.setLayout(self.preview_layout)

        self.preview_table = 0
        self.preview_row = 0
        self.preview_column = 1

        # Invisible preview layout holds unused preview (widget becomes a window if no parent and set invisible is slow)
        self.hidden_preview_layout = QHBoxLayout()
        self.hidden_preview_widget = QWidget()
        self.hidden_preview_widget.setLayout(self.hidden_preview_layout)
        self.hidden_preview_widget.setVisible(False)

        # Empty slot image #
        self.empty_slot_image = QPixmap.fromImage(ImageQt(Image.open("Template/Card Back Grayscale.png")).scaledToWidth(100, QtCore.Qt.SmoothTransformation))

        # Selected card border #
        self.selected_border = Image.open("Template/Card Selected.png")

        # Create card button #
        create_card_button = QPushButton(self.locale["create_card"])
        create_card_button.clicked.connect(self.createCardClicked)
        self.preview_panel.addWidget(create_card_button)

        # Previous / next page buttons #
        self.page_buttons_widget = QWidget()
        page_button_layout = QHBoxLayout()
        previous_preview_page_button = QPushButton("<=")
        previous_preview_page_button.clicked.connect(self.previousPreviewTableClicked)
        page_button_layout.addWidget(previous_preview_page_button)
        next_preview_page_button = QPushButton("=>")
        next_preview_page_button.clicked.connect(self.nextPreviewTableClicked)
        page_button_layout.addWidget(next_preview_page_button)
        self.page_buttons_widget.setLayout(page_button_layout)
        self.preview_panel.addWidget(self.page_buttons_widget)

        # Add preview zone #
        self.preview_panel.addWidget(preview_widget)

        # Delete card button #
        delete_card_button = QPushButton(self.locale["delete_card"])
        delete_card_button.clicked.connect(self.deleteCardClicked)
        self.preview_panel.addWidget(delete_card_button)

        # Set first card preview #
        self.card_preview = CardPreview(self.card)
        self.card_preview.toggle_signal.connect(self.previewClicked)
        self.preview_array.append(self.card_preview)
        self.preview_layout.addWidget(self.card_preview, 0, 0)

        # Lock generation #
        self.lockGeneration()

        # Toggle default clan #
        self.clan_button_dict["None"].activate()
        self.clanToggled("None")

        # Create border frame #
        self.border_frame = QWidget()
        self.border_frame.setWindowTitle(self.locale["border"])
        border_frame_layout = QHBoxLayout()
        self.border_frame.setLayout(border_frame_layout)
        self.border_color_dialog = QColorDialog()
        self.border_color_dialog.setOption(QColorDialog.NoButtons)
        border_frame_layout.addWidget(self.border_color_dialog)
        self.border_color_dialog.currentColorChanged.connect(self.borderColorChanged)

        # Create resize frame #
        self.resize_art_frame = QWidget()
        self.resize_art_frame.setWindowTitle(self.locale["resize_art"])
        resize_art_frame_layout = QHBoxLayout()
        self.resize_art_frame.setLayout(resize_art_frame_layout)

        # Card handler init #
        self.card_art_handler = CardArtHandler(self)
        resize_art_frame_layout.addWidget(self.card_art_handler)

        # Create help frame #
        self.help_frame = QWidget()
        self.help_frame.setWindowTitle(self.locale["help"])
        help_frame_layout = QHBoxLayout()
        self.help_frame.setLayout(help_frame_layout)

        left_help_frame_layout = QVBoxLayout()
        help_frame_layout.addLayout(left_help_frame_layout)
        right_help_frame_layout = QVBoxLayout()
        help_frame_layout.addLayout(right_help_frame_layout)

        self.help_text = QLabel(self.locale["help_text"])
        left_help_frame_layout.addWidget(self.help_text)

        # Load base card state #
        self.loadCardState()

        # Unlock generation #
        self.unlockGeneration()

        # Set art handler default art and generate #
        dummy_image = Image.open("Template/Dummy.png")
        image = QPixmap.fromImage(ImageQt(dummy_image).scaledToHeight(500, QtCore.Qt.SmoothTransformation))
        self.card_art_handler.setArt(image, dummy_image.height)
        dummy_image.close()

    def run(self):
        # Draw default GUI #
        self.drawGUI()
        # Show the window #
        self.main_frame.showMaximized()

        # Run the main Qt loop #
        self.app.exec()

    def clear(self, layout):
        for index in reversed(range(layout.count())):
            widget_to_remove = layout.itemAt(index).widget()
            if isinstance(widget_to_remove, QWidget):
                widget_to_remove.setParent(None)

    def clearGUI(self):
        self.clear(self.left_panel)
        self.clear(self.middle_panel)
        self.clear(self.right_panel)

        self.left_rows = [0, 0]
        self.middle_rows = [0, 0]
        self.right_rows = [0]

    def draw(self, layout, widget, rows_number, column):
        widget_alignment = QtCore.Qt.AlignBaseline
        if column == 0  and layout != self.right_panel:
            widget_alignment = QtCore.Qt.AlignRight
        if widget == self.card_image:
            widget_alignment = QtCore.Qt.AlignHCenter
        layout.addWidget(widget, rows_number[column], column, alignment=widget_alignment)
        rows_number[column] += 1

    def drawGUI(self):
        # Draw default widgets #

        # Name #
        self.draw(self.left_panel, self.name_label, self.left_rows, 0)
        self.draw(self.left_panel, self.name_text, self.left_rows, 1)
        self.draw(self.right_panel, self.image_buttons, self.right_rows, 0)
        self.draw(self.right_panel, self.card_image, self.right_rows, 0)
        self.draw(self.right_panel, self.artist_widget, self.right_rows, 0)
        self.draw(self.right_panel, self.copyright_widget, self.right_rows, 0)
        self.draw(self.right_panel, self.border_widget, self.right_rows, 0)
        self.draw(self.right_panel, self.json_load_button, self.right_rows, 0)
        self.draw(self.right_panel, self.json_save_button, self.right_rows, 0)
        self.draw(self.right_panel, self.json_save_as_button, self.right_rows, 0)
        self.draw(self.right_panel, self.json_import_button, self.right_rows, 0)
        self.draw(self.right_panel, self.language_font_help_widget, self.right_rows, 0)

        card_type_index = self.type_combo_box.currentIndex()

        # Draw type dependant widgets #

        # Vampire #
        if card_type_index == 0:
            self.draw(self.left_panel, self.clan_label, self.left_rows, 0)
            self.draw(self.left_panel, self.clan_icons, self.left_rows, 1)
            self.draw(self.left_panel, self.capacity_label, self.left_rows, 0)
            self.draw(self.left_panel, self.capacity_text, self.left_rows, 1)
            self.draw(self.left_panel, self.block_number_label, self.left_rows, 0)
            self.draw(self.left_panel, self.block_number_text, self.left_rows, 1)
            self.draw(self.middle_panel, self.discipline_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.discipline_icons, self.middle_rows, 1)
            self.draw(self.middle_panel, self.advanced_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.advanced_checkbox, self.middle_rows, 1)


        # Minion Type #
        elif card_type_index == 1:
            self.draw(self.left_panel, self.blood_label, self.left_rows, 0)
            self.draw(self.left_panel, self.blood_text, self.left_rows, 1)
            self.draw(self.left_panel, self.pool_label, self.left_rows, 0)
            self.draw(self.left_panel, self.pool_text, self.left_rows, 1)
            self.draw(self.left_panel, self.capacity_label, self.left_rows, 0)
            self.draw(self.left_panel, self.capacity_text, self.left_rows, 1)
            self.draw(self.left_panel, self.minion_type_label, self.left_rows, 0)
            self.draw(self.left_panel, self.minion_type_icons, self.left_rows, 1)
            self.draw(self.left_panel, self.clan_label, self.left_rows, 0)
            self.draw(self.left_panel, self.clan_icons, self.left_rows, 1)
            self.draw(self.middle_panel, self.discipline_label, self.middle_rows, 0)
            self.draw(self.middle_panel, self.discipline_icons, self.middle_rows, 1)

        # Minion Type #
      #  elif card_type_index == 2:
       #     self.draw(self.left_panel, self.cost_label, self.left_rows, 0)
      #      self.draw(self.left_panel, self.cost_text, self.left_rows, 1)
      #      self.draw(self.left_panel, self.minion_type_label, self.left_rows, 0)
       #     self.draw(self.left_panel, self.minion_type_icons, self.left_rows, 1)
      #      self.draw(self.left_panel, self.clan_label, self.left_rows, 0)
      #      self.draw(self.left_panel, self.clan_icons, self.left_rows, 1)
       #     self.draw(self.middle_panel, self.discipline_label, self.middle_rows, 0)
       #     self.draw(self.middle_panel, self.discipline_icons, self.middle_rows, 1)


        self.draw(self.left_panel, self.effect_label, self.left_rows, 0)
        self.draw(self.left_panel, self.effect_text, self.left_rows, 1)
        self.draw(self.middle_panel, self.flavour_label, self.middle_rows, 0)
        self.draw(self.middle_panel, self.flavour_text, self.middle_rows, 1)
        self.draw(self.left_panel, self.type_label, self.left_rows, 0)
        self.draw(self.left_panel, self.type_combo_box, self.left_rows, 1)
        self.draw(self.left_panel, self.variant_label, self.left_rows, 0)
        self.draw(self.left_panel, self.variant_combo_box, self.left_rows, 1)
   #     self.draw(self.left_panel, self.background_label, self.left_rows, 0)
    #    self.draw(self.left_panel, self.background_combo_box, self.left_rows, 1)


     #   self.draw(self.middle_panel, self.set_name_label, self.middle_rows, 0)
    #    self.draw(self.middle_panel, self.set_name_text, self.middle_rows, 1)
        self.draw(self.middle_panel, self.set_number_label, self.middle_rows, 0)
        self.draw(self.middle_panel, self.set_number_widget, self.middle_rows, 1)

    def redrawPreview(self, manual = False):
        if not manual:
            self.preview_table = int(self.preview_array.index(self.card_preview) / 24)
        self.preview_row = 0
        self.preview_column = 0

        self.clear(self.preview_layout)
        self.clear(self.hidden_preview_layout)

        preview_max_range = min(len(self.preview_array), (self.preview_table + 1) * 24)
        preview_range = range(self.preview_table * 24, preview_max_range)

        for preview_index in range(len(self.preview_array)):
            preview = self.preview_array[preview_index]
            if preview_index in preview_range:
                if self.preview_column == 4:
                    self.preview_row += 1
                    self.preview_column = 0
                self.preview_layout.addWidget(preview, self.preview_row, self.preview_column)
                self.preview_column += 1
            else:
                self.hidden_preview_layout.addWidget(preview)

        while self.preview_row != 5 or self.preview_column != 4:
            if self.preview_column == 4:
                self.preview_row += 1
                self.preview_column = 0
            self.preview_layout.addWidget(self.emptySlotLabel(), self.preview_row, self.preview_column)
            self.preview_column += 1

        self.previewClicked(self.card_preview)

    def emptySlotLabel(self):
        empty_slot_label = QLabel()
        empty_slot_label.setPixmap(self.empty_slot_image)

        return empty_slot_label

    def nameChanged(self):
        self.card.name = self.name_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def cardTypeChanged(self):
        type_index = self.type_combo_box.currentIndex()

        for index in reversed(range(self.variant_combo_box.count())):
            self.variant_combo_box.removeItem(index)

        if type_index == 0:

            self.variant_combo_box.addItems(self.locale["variant_names"][0:4])
       #     self.card.variant = self.variant_names[0]
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Vampire"))
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Alternate Vampire"))
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Classic Vampire"))
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Old Vampire"))
        elif type_index == 1:
            self.variant_combo_box.addItems(self.locale["variant_names"][0:4])
            self.card.variant = self.variant_names[0]
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Minion"))
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Alternate Minion"))
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Classic Minion"))
            self.variant_combo_box.addItems(os.listdir("Template/Custom/Old Minion"))

        self.card.card_type = self.type_names[type_index]

        self.clearGUI()
        self.drawGUI()

        if self.lockGeneration():
            self.card_art_handler.computeRubberband(False, True)
            self.generateCard()
            self.unlockGeneration()


    def variantChanged(self):
        if self.variant_combo_box.currentText():
            if self.variant_combo_box.currentText() in self.locale["variant_names"]:
                variant_index = self.locale["variant_names"].index(self.variant_combo_box.currentText())
                self.card.variant = self.variant_names[variant_index]
            else:
                self.card.variant = self.variant_combo_box.currentText()

        if self.lockGeneration():
            self.card_art_handler.computeRubberband(False, True)
            self.generateCard()
            self.unlockGeneration()

    def clanToggled(self, name):
        button = self.clan_button_dict[name]
        if name != self.current_clan_button.name:
            self.current_clan_button.toggled = False
            self.current_clan_button.setPixmap(self.current_clan_button.inactive)
            self.current_clan_button = button
        if button.toggled:
            self.card.clan = name
        else:
            self.card.clan = ""

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def minion_typeToggled(self, name):
        button = self.minion_type_button_dict[name]
        if name != self.current_minion_type_button.name:
            self.current_minion_type_button.toggled = False
            self.current_minion_type_button.setPixmap(self.current_minion_type_button.inactive)
            self.current_minion_type_button = button
        if button.toggled:
            self.card.minion_type = name
        else:
            self.card.minion_type = ""

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def bloodChanged(self):
        self.card.blood = self.blood_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def poolChanged(self):
        self.card.pool = self.pool_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def capacityChanged(self):
        self.card.capacity = self.capacity_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def advancedChanged(self):
        self.card.advanced = self.advanced_checkbox.isChecked()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()


    def effectChanged(self):
        self.card.effect = self.effect_text.toPlainText()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def flavourChanged(self):
        self.card.flavour = self.flavour_text.toPlainText()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def backgroundChanged(self):
        self.card.background_path = "Template/Custom/Background/" + self.background_combo_box.currentText()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()


    def blockNumberChanged(self):
        self.card.block_number = self.block_number_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def setNameChanged(self):
        self.card.set_name = self.set_name_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def setNumberChanged(self):
        if not self.set_number_text.text().isdigit():
            self.set_number_text.setText("")
            return

        self.card.set_number = self.set_number_text.text()

        preview_set = False

        self.preview_array.remove(self.card_preview)
        if not self.card.set_number:
            self.card.set_number = "0"
            self.preview_array.insert(0, self.card_preview)
            preview_set = True
        else:
            for index, preview in enumerate(self.preview_array):
                if int(preview.card.set_number) >= int(self.card.set_number):
                    self.preview_array.insert(index, self.card_preview)
                    preview_set = True
                    break

        if not preview_set:
            self.preview_array.append(self.card_preview)

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

        self.redrawPreview()

    def setMaxNumberChanged(self):
        if not self.set_max_number_text.text().isdigit():
            self.set_max_number_text.setText("")
            return

        self.card.set_max_number = self.set_max_number_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def artistChanged(self):
        self.card.artist = self.artist_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def copyrightChanged(self):
        self.card.copyright = self.copyright_text.text()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def borderSharpChanged(self):
        self.card.border_sharp = self.border_sharp_checkbox.isChecked()

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def bleedingXChanged(self):
        if self.bleeding_x_text.text():
            if not self.bleeding_x_text.text().isdigit():
                self.bleeding_x_text.setText("")
            else:
                if self.bleeding[0] == 1500:
                    return

                self.bleeding[0] = min(1500, int(self.bleeding_x_text.text()))
        else:
            if self.bleeding[0] == 752:
                return

            self.bleeding[0] = 752

        if self.lockGeneration():
            for preview in self.preview_array:
                self.generateCard(preview, self.card_preview == preview)
            self.unlockGeneration()

    def bleedingYChanged(self):
        if self.bleeding_y_text.text():
            if not self.bleeding_y_text.text().isdigit():
                self.bleeding_y_text.setText("")
            else:
                if self.bleeding[1] == 2100:
                    return

                self.bleeding[1] = min(2100, int(self.bleeding_y_text.text()))
        else:
            if self.bleeding[1] == 1050:
                return

            self.bleeding[1] = 1050

        if self.lockGeneration():
            for preview in self.preview_array:
                self.generateCard(preview, self.card_preview == preview)
            self.unlockGeneration()

    def disciplineToggled(self, name):
        if name not in self.card.discipline_icons:
            self.card.discipline_icons.append(name)
        else:
            self.card.discipline_icons.remove(name)

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def selectArt(self):
        filename, _ = QFileDialog.getOpenFileName(dir=self.config["default_directory"], caption="Select Art File", filter=("Images (*.png *.jpg *.jpeg *.webp);;All files (*)"))
        if filename:
            tf = tempfile.NamedTemporaryFile(suffix=pathlib.Path(filename).suffix, delete=True).name
            shutil.copyfile(filename, tf)
            self.card.art_path = tf
            pil_image = Image.open(tf)
            image = QPixmap.fromImage(ImageQt(pil_image).scaledToHeight(500, QtCore.Qt.SmoothTransformation))
            self.card_art_handler.setArt(image, pil_image.height)
            pil_image.close()
        else:
            print("Invalid art file!")

    def resizeArt(self):
        if self.resize_art_frame.isVisible():
            self.resize_art_frame.hide()
        else:
            self.resize_art_frame.show()

    def generateCard(self, card_preview=None, switch_select=True):
        if not card_preview:
            card_preview = self.card_preview
        
        generated_image = self.wcg.createCard(card_preview.card, self.bleeding).copy()

        if card_preview == self.card_preview:
            full_image = ImageQt(generated_image).scaledToHeight(500, QtCore.Qt.SmoothTransformation)
            full_image = QPixmap.fromImage(full_image)
            self.card_image.setPixmap(full_image)

        preview_image = QPixmap.fromImage(ImageQt(generated_image))
        preview_image = preview_image.scaledToWidth(100, QtCore.Qt.SmoothTransformation)
        card_preview.preview_image = preview_image

        if switch_select:
            generated_image.alpha_composite(self.selected_border.resize((max(720, self.bleeding[0]), max(1050, self.bleeding[1])), 1), (0, 0))
        generated_image = QPixmap.fromImage(ImageQt(generated_image))
        generated_image = generated_image.scaledToWidth(100, QtCore.Qt.SmoothTransformation)
        card_preview.selected_preview_image = generated_image
        
        card_preview.setPixmap(generated_image)

    def saveCard(self):
        filename, _ = QFileDialog.getSaveFileName(dir=self.config["default_directory"], caption="Select Card Save Location", filter=("Image (*.png)"))
        if filename:
            save_path = filename
            if filename[-4:] != ".png":
                save_path += ".png"
            self.card.image.save(save_path)
        else:
            print("Invalid save path!")

    def saveAllCards(self):
        save_path = QFileDialog.getExistingDirectory(dir=self.config["default_directory"], caption="Select a save directory")
        if save_path:
            default_name = 0
            for preview in self.preview_array:
                card = preview.card
                if card.name:
                    card_name = card.name
                else:
                    card_name = str(default_name)
                    default_name += 1
                
                self.wcg.createCard(card, self.bleeding).save(save_path + "/" + card_name + ".png")
        else:
            print("Invalid save path!")

    def saveToPrintable(self):
        save_path = QFileDialog.getExistingDirectory(dir=self.config["default_directory"], caption="Select a save directory")
        if save_path:
            default_name = 0
            printable_image = Image.new("RGBA", (self.bleeding[0] * 3, self.bleeding[1] * 3))
            pending = False
            for index, preview in enumerate(self.preview_array):
                pending = True
                card = preview.card
                if card.name:
                    card_name = card.name
                else:
                    card_name = str(default_name)
                    default_name += 1

                self.wcg.createCard(card, self.bleeding)
                printable_image.alpha_composite(card.image, (self.bleeding[0]  * (index % 3), self.bleeding[1] * int((index % 9) / 3)))

                if index % 9 == 8:
                    printable_image.save(save_path + "/" + "printable_" + str(int(index / 9)) + ".png")
                    printable_image = Image.new("RGBA", (self.bleeding[0] * 3, self.bleeding[1] * 3))
                    pending = False

            if pending:
                printable_image.save(save_path + "/" + "printable_" + str(int(len(self.preview_array) / 9)) + ".png")
        else:
            print("Invalid save path!")

    # same as load but does not clear everything plus ignores conflicted uuids
    def importFromJSON(self):
        json_path, _ = QFileDialog.getOpenFileName(dir=self.config["default_directory"], caption="Select JSON File", filter=("JSON (*.json);;All files (*)"))
        if os.path.isfile(json_path):
            art_base_path = json_path[:-5] + " Arts/"

            uuids = set()
            for card in self.preview_array:
                uuids.add(card.card.uuid)

            try:
                with open(json_path, "r", encoding="utf-8") as card_file:
                    card_dict = json.load(card_file)
                    for card_key in card_dict:
                        card_json = card_dict[card_key]
                        card = Card()
                        card.fromJson(card_json)
                        if card.uuid in uuids:
                            card.uuid = str(uuid.uuid4()) # uuid already in, make a duplicate and change uuid

                        if card.art_path != "Template/Dummy.png":
                            card.art_path = art_base_path + card.art_path
                        self.createCard(card)

                self.redrawPreview()
                QMessageBox.information(self.main_frame, "Load Json", "Json file loaded.")

            except Exception as e:
                QMessageBox.information(self.main_frame, "Load Json", "Could not load Json file.\nError is : " + str(e))
                print("Could not load Json file at:", json_path)
        else:
            print("Invalid Json file path!")

    def loadFromJSON(self):
        json_path, _ = QFileDialog.getOpenFileName(dir=self.config["default_directory"], caption="Select JSON File", filter=("JSON (*.json);;All files (*)"))
        if os.path.isfile(json_path):
            self.preview_array = []

            art_base_path = json_path[:-5] + " Arts/"

            try:
                with open(json_path, "r", encoding="utf-8") as card_file:
                    card_dict = json.load(card_file)
                    for card_key in card_dict:
                        card_json = card_dict[card_key]
                        card = Card()
                        card.fromJson(card_json)
                        if card.art_path != "Template/Dummy.png":
                            card.art_path = art_base_path + card.art_path
                        self.createCard(card)

                self.last_save_path = json_path
                self.previewClicked(self.preview_array[-1])

                QMessageBox.information(self.main_frame, "Load Json", "Json file loaded.")

            except Exception as e:
                QMessageBox.information(self.main_frame, "Load Json", "Could not load Json file.\nError is : " + str(e))
                print("Could not load Json file at:", json_path)

                if len(self.preview_array) == 0:
                    self.createCard(Card())

            self.redrawPreview()
            self.previewClicked(self.preview_array[-1])
        else:
            print("Invalid Json file path!")

    def saveToJSON(self):
        if self.last_save_path is not None:
            self.saveToJSONAs(save_path=self.last_save_path)
        else:
            self.saveToJSONAs()

    def saveToJSONAs(self, save_path=None, *args):
        if save_path is None:
            save_path, _ = QFileDialog.getSaveFileName(dir=self.config["default_directory"], caption="Select JSON Save Location", filter=("JSON (*.json);;All files (*)"))

        if len(save_path) == 0:
            return

        local_art_folder_path = os.path.basename(save_path)

        if save_path[-5:] != ".json":
            save_path = save_path + ".json"
        else:
            local_art_folder_path = local_art_folder_path[:-5]

        self.last_save_path = save_path

        local_art_folder_path += " Arts/"

        save_art_folder = save_path[:-5] + " Arts"
        if not os.path.isdir(save_art_folder):
            os.mkdir(save_art_folder)

        last_card_name = ""

        try:
            with open(save_path + ".tmp", "w", encoding="utf-8") as json_file:
                json_card_list = {}
                card_id = 0
                for preview in self.preview_array:
                    card = preview.card
                    last_card_name = card.name
                    if card.art_path != "Template/Dummy.png":
                        art_new_save_path = card.uuid + pathlib.Path(card.art_path).suffix
                        art_save_path = save_art_folder + "/" + art_new_save_path
                        if card.art_path != art_save_path:
                            shutil.copy(card.art_path, art_save_path)
                    json_card_list[card_id] = card.toJson()
                    if card.art_path != "Template/Dummy.png":
                        json_card_list[card_id]["art_path"] = art_new_save_path
                    card_id += 1
                json.dump(json_card_list, json_file, indent="\t")

            shutil.copy(save_path + ".tmp", save_path)
            os.remove(save_path + ".tmp") 
            QMessageBox.information(self.main_frame, "Save Json", "Json file saved.")

        except Exception:
            QMessageBox.information(self.main_frame, "Save Json", "Could not save Json file.")
            print("Could not save Json because of", last_card_name)

    def languageChanged(self):
        locale_file = open("Locales/" + self.language_combo_box.currentText() + ".json", "r", encoding="utf-8")
        self.locale = json.load(locale_file)
        locale_file.close()

        self.lockGeneration()

        self.name_label.setText(self.locale["name"])
        self.type_label.setText(self.locale["type"])
        self.variant_label.setText(self.locale["variant"])
        self.blood_label.setText(self.locale["blood"])
        self.pool_label.setText(self.locale["pool"])
        self.clan_label.setText(self.locale["clan"])
        self.minion_type_label.setText(self.locale["minion_type"])
        self.capacity_label.setText(self.locale["capacity"])
        self.effect_label.setText(self.locale["effect"])
        self.flavour_label.setText(self.locale["flavour"])
        self.discipline_label.setText(self.locale["disciplines"])
        self.block_number_label.setText(self.locale["block_number"])
        self.set_name_label.setText(self.locale["set_name"])
        self.set_number_label.setText(self.locale["set_number"])
        self.import_art_button.setText(self.locale["import_art"])
        self.resize_art_button.setText(self.locale["resize_art"])
        self.save_card_button.setText(self.locale["save_card"])
        self.save_to_printable_button.setText(self.locale["save_to_printable"])
        self.artist_label.setText(self.locale["artist"])
        self.copyright_label.setText(self.locale["copyright"])
        self.border_sharp_label.setText(self.locale["sharp"])
        self.border_label.setText(self.locale["border"])
        self.bleeding_label.setText(self.locale["bleeding"])
        self.json_save_button.setText(self.locale["save_json"])
        self.json_save_as_button.setText(self.locale["save_json_as"])
        self.json_load_button.setText(self.locale["load_json"])
        self.json_import_button.setText(self.locale["import_json"])
        self.language_label.setText(self.locale["language"])
        self.font_label.setText(self.locale["font"])
        self.help_button.setText(self.locale["help"])
        self.help_text.setText(self.locale["help_text"])

        # Store combobox indexes #
        old_indexes = []

        # Change Type language #
        old_indexes.append(self.type_combo_box.currentIndex())
        old_indexes.append(self.variant_combo_box.currentIndex())
        for index in reversed(range(self.type_combo_box.count())):
            self.type_combo_box.removeItem(index)
        self.type_combo_box.addItems(self.locale["type_names"])

        self.unlockGeneration()

        # Check if language changed by user input or preview click #
        if not self.nonManualLanguageSwitch:
            # Restore combobox indexes #
            self.type_combo_box.setCurrentIndex(old_indexes[0])
            self.variant_combo_box.setCurrentIndex(old_indexes[1])

        self.card.locale_name = self.language_combo_box.currentText()

        self.generateCard()

    def borderClicked(self):
        if self.border_frame.isVisible():
            self.border_frame.hide()
        else:
            self.border_frame.show()

    def borderColorChanged(self, color):
        self.card.border_color = (color.red(), color.green(), color.blue(), 255)
        self.border_color_button.setStyleSheet("QPushButton{background-color : #" + "%0.2x" % color.red() + "%0.2x" % color.green() + "%0.2x" % color.blue() + ";}")

        if self.lockGeneration():
            self.generateCard()
            self.unlockGeneration()

    def helpClicked(self):
        if self.help_frame.isVisible():
            self.help_frame.hide()
        else:
            self.help_frame.show()

    def createCard(self, card = None):
        if card:
            new_card = card
        else:
            new_card = Card()
            new_card.locale_name = self.card.locale_name

        new_card_preview = CardPreview(new_card)
        new_card_preview.toggle_signal.connect(self.previewClicked)
        
        if not card:
            # Keep card type #
            new_card.card_type = self.card.card_type
            # Keep clan #
            new_card.clan = self.card.clan
            # Keep minion type #
            new_card.minion_type = self.card.minion_type
            # Keep variant #
            new_card.variant = self.card.variant
            # Keep and update set related informations #
            new_card.block_number = self.card.block_number
            new_card.set_name = self.card.set_name
            new_card.set_number = str(len(self.preview_array) + 1)
            set_size = new_card.set_number
            str_set_size = str(set_size)
            new_card.set_max_number = str_set_size
            for preview in self.preview_array:
                preview.card.set_max_number = str_set_size

            # Keep border style #
            new_card.border_color = self.card.border_color

        else:
            self.nonManualLanguageSwitch = True

        self.generateCard(new_card_preview)

        preview_set = False
        
        if card:
            new_card_preview.setPixmap(new_card_preview.preview_image)
            if not self.card.set_number:
                self.card.set_number = "0"
                self.preview_array.insert(0, new_card_preview)
                preview_set = True
            else:
                for index in range(len(self.preview_array)):
                    if int(self.preview_array[index].card.set_number) >= int(new_card_preview.card.set_number):
                        self.preview_array.insert(index, new_card_preview)
                        preview_set = True
                        break

        if not preview_set:
            self.preview_array.append(new_card_preview)

        self.nonManualLanguageSwitch = False

    def createCardClicked(self):
        self.createCard()
        self.redrawPreview()
        self.previewClicked(self.preview_array[-1])

    def deleteCardClicked(self):
        preview_size = len(self.preview_array)
        # Set preview to previous card #
        preview_index = self.preview_array.index(self.card_preview)
        if preview_index > 0:
            preview_index -= 1
        self.preview_array.remove(self.card_preview)

        if preview_size == 1:
            self.createCard(Card())

        # reorganize previews
        it = 0
        total_in_set = len(self.preview_array)
        for preview in self.preview_array:
            card = preview.card
            card.set_number = str(it)
            card.set_max_number = str(total_in_set)
            it = it + 1

        self.previewClicked(self.preview_array[preview_index])
        self.redrawPreview()

    def previousPreviewTableClicked(self):
        if self.preview_table > 0:
            self.preview_table -= 1
            self.redrawPreview(True)

    def nextPreviewTableClicked(self):
        if self.preview_table < int((len(self.preview_array) - 1) / 24):
            self.preview_table += 1
            self.redrawPreview(True)

    def previewClicked(self, card_preview):
        changed = card_preview != self.card_preview
        if changed:
            self.card_preview.setPixmap(self.card_preview.preview_image)
            self.card_preview = card_preview
            self.card = self.card_preview.card

            pil_image = Image.open(self.card.art_path)
            image = QPixmap.fromImage(ImageQt(pil_image).scaledToHeight(500, QtCore.Qt.SmoothTransformation))
            self.card_art_handler.setArt(image, pil_image.height, False)
            ratio = pil_image.height / 500
            pil_image.close()

            if self.card.art_box:
                self.card_art_handler.ax = self.card.art_box[0] / ratio
                self.card_art_handler.ay = self.card.art_box[1] / ratio
                self.card_art_handler.bx = self.card.art_box[2] / ratio
                self.card_art_handler.by = self.card.art_box[3] / ratio

                # Safeguard for ratio computation when loading #
                if self.card_art_handler.ax < 0:
                    self.card_art_handler.ax = 0
                if self.card_art_handler.ay < 0:
                    self.card_art_handler.ax = 0
                if self.card_art_handler.bx >= self.card_art_handler.max_x:
                    self.card_art_handler.bx = self.card_art_handler.max_x - 1
                if self.card_art_handler.by >= self.card_art_handler.max_y:
                    self.card_art_handler.by = self.card_art_handler.max_y - 1

            self.card_art_handler.computeRubberband(True, True)

            self.generateCard()

        if len(self.preview_array) == 1 or changed:
            self.nonManualLanguageSwitch = True

            self.loadCardState()

            self.nonManualLanguageSwitch = False

    def defaultDirectoryClicked(self):
        selected_directory = QFileDialog.getExistingDirectory()
        if(selected_directory):
            self.config["default_directory"] = selected_directory
            config_file = open("config.json", "w+", encoding="utf-8")
            json.dump(self.config, config_file, indent="\t")
            config_file.close()

    def fontMinusButtonClicked(self):
        font = self.app.font()
        self.font_size = max(1, self.font_size - 1)
        font.setPointSize(self.font_size)
        self.app.setFont(font)

        self.config["font_size"] = self.font_size
        config_file = open("config.json", "w+", encoding="utf-8")
        json.dump(self.config, config_file, indent="\t")
        config_file.close()

    def fontPlusButtonClicked(self):
        font = self.app.font()
        self.font_size = min(40, self.font_size + 1)
        font.setPointSize(self.font_size)
        self.app.setFont(font)

        self.config["font_size"] = self.font_size
        config_file = open("config.json", "w+", encoding="utf-8")
        json.dump(self.config, config_file, indent="\t")
        config_file.close()


    def loadCardState(self):
        self.lockGeneration()
        # Name #
        self.name_text.setText(self.card.name)
        # Language #
        res = self.language_combo_box.findText(self.card.locale_name)
        if res < 0:
            res = 0
        self.language_combo_box.setCurrentIndex(res)
        # Store variant #
        variant = self.card.variant
        # Type #
        if self.card.card_type in self.type_names:
            res = self.type_combo_box.findText(self.locale["type_names"][self.type_names.index(self.card.card_type)])
        else:
            res = 0
        self.type_combo_box.setCurrentIndex(res)
        # Clan #
        if self.card.clan in self.clan_button_dict.keys():
            self.clan_button_dict[self.card.clan].mousePressEvent(None)
        else:
            self.clan_button_dict["None"].mousePressEvent(None)
        # Minion type #
        if self.card.minion_type in self.minion_type_button_dict.keys():
            self.minion_type_button_dict[self.card.minion_type].mousePressEvent(None)
        else:
            self.minion_type_button_dict["Action"].mousePressEvent(None)
        # Trigger type changed #
        self.cardTypeChanged()
        # Reapply variant #
        self.card.variant = self.card.variant
        if variant == "Classic":
            self.variant_combo_box.setCurrentIndex(0)
        elif variant == "FullBleed":
            self.variant_combo_box.setCurrentIndex(1)
        elif variant == "Old":
            self.variant_combo_box.setCurrentIndex(3)
        else:
            if self.card.variant in self.locale["variant_names"]:
                self.variant_combo_box.setCurrentIndex(self.variant_combo_box.findText(self.locale["variant_names"][self.variant_names.index(self.card.variant)]))
            else:
                res = self.variant_combo_box.findText(self.card.variant)
                if res < 0:
                    res = 0
                self.variant_combo_box.setCurrentIndex(res)
        # Disciplinees #
        for key in self.discipline_button_dict:
            if key in self.card.discipline_icons:
                self.discipline_button_dict[key].activate()
            else:
                self.discipline_button_dict[key].deactivate()
        self.blood_text.setText(self.card.blood)
        self.pool_text.setText(self.card.pool)
        self.capacity_text.setText(self.card.capacity)
        # Use plain text to avoid interpretation of bold, italic, etc #
        self.effect_text.setPlainText(self.card.effect)
        self.flavour_text.setPlainText(self.card.flavour)
        self.artist_text.setText(self.card.artist)
        self.copyright_text.setText(self.card.copyright)
        self.block_number_text.setText(self.card.block_number)
        self.set_name_text.setText(self.card.set_name)
        self.set_number_text.setText(self.card.set_number)
        self.set_max_number_text.setText(self.card.set_max_number)
        self.border_color_dialog.setCurrentColor(QColor(self.card.border_color[0], self.card.border_color[1], self.card.border_color[2]))
        self.border_color_button.setStyleSheet("QPushButton{background-color : #" + "%0.2x" % self.card.border_color[0] + "%0.2x" % self.card.border_color[1] + "%0.2x" % self.card.border_color[2] + ";}")
        self.border_sharp_checkbox.setChecked(self.card.border_sharp)

        self.unlockGeneration()

    def lockGeneration(self):
        if self.lock_generation:
            return False
        else:
            self.lock_generation = True
            return True

    def unlockGeneration(self):
        self.lock_generation = False

WoWCardGeneratorGUI().run()
