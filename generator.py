#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PIL import Image, ImageDraw, ImageFont, ImageOps
from math import ceil, floor
import json
import re
import uuid

from os.path import exists, isfile

type_names = ["Vampire", "Minion"]
variant_names = ["Classic", "FullBleed", "Alternate", "Old"]
clan_names = ["None", "Banu Haquim", "Brujah", "Gangrel", "Hecata", "Lasombra", "Malkavian", "Ministry", "Nosferatu", "Ravnos", "Salubri", "Toreador", "Tremere", "Tzimisce", "Ventrue"]
minion_type_names = ["None", "Action", "Reaction", "Ally", "Combat", "Modifier", "ModifierReaction", "ModifierCombat", "ActionCombat", "ActionReaction", "CombatReaction", "Event", "Retainer", "CombatRetainer",  "Equipment", "CombatEquipment", "ModifierAlly", "ReactionRetainer", "Political", "Master"]
disciplines_names = ["Flight", "Inferior Abombwe", "Inferior Animalism", "Inferior Auspex", "Inferior Blood Sorcery", "Inferior Celerity", "Inferior Chimerstry", "Inferior Daimonion", "Inferior Dementation", "Inferior Dominate", "Inferior Fortitude", "Inferior Melopomine", "Inferior Mytherceria", "Inferior Necromancy", "Inferior Obeah", "Inferior Obfuscate", "Inferior Oblivion", "Inferior Obtenebration", "Inferior Potence", "Inferior Presence", "Inferior Protean", "Inferior Quietus", "Inferior Sanguinus", "Inferior Serpentis", "Inferior Spiritus", "Inferior Temporis", "Inferior Thaumaturgy", "Inferior Thanatosis", "Inferior Valeren", "Inferior Vicissitude", "Inferior Visceratika", "Superior Abombwe", "Superior Animalism", "Superior Auspex", "Superior Blood Sorcery", "Superior Celerity", "Superior Chimerstry", "Superior Daimonion", "Superior Dementation", "Superior Dominate", "Superior Fortitude", "Superior Melopomine", "Superior Mytherceria", "Superior Necromancy", "Superior Obeah", "Superior Obfuscate", "Superior Oblivion", "Superior Obtenebration", "Superior Potence", "Superior Presence", "Superior Protean", "Superior Quietus", "Superior Sanguinus", "Superior Serpentis", "Superior Spiritus", "Superior Temporis", "Superior Thaumaturgy", "Superior Thanatosis", "Superior Valeren", "Superior Vicissitude", "Superior Visceratika"]


class Card(object):
    def __init__(self):
        # Define card values #
        self.uuid = str(uuid.uuid4())
        self.name = ""
        self.card_type = "Vampire"
        self.variant = "Classic"
        self.clan = "None"
        self.minion_type = "Action"
        self.discipline_icons = []
        self.blood = ""
        self.pool = ""
        self.capacity = ""
        self.advanced = "False"
        self.effect = ""
        self.art_box = (0, 54, 400, 600)
        self.art = ""
        self.flavour = ""
        self.artist = ""
        self.copyright = "©2024 Paradox Interactive AB (publ)"
        self.block_number = ""
        self.set_name = ""
        self.set_number = "1"
        self.set_max_number = "1"
        self.border_color = (0, 0, 0, 255)
        self.border_sharp = False
        self.locale_name = "English"
        self.art_path = "Template/Dummy.png"
        self.background_path = ""
        self.locale = {}
        self.image = None

    def get_from_json(self, json_object, field):
        if field in json_object:
            return json_object[field]
        else:
            return ""

    def fromJson(self, json_object):  # Json object is indeed a dict
        self.uuid = self.get_from_json(json_object, "uuid")
        if self.uuid == "":
            self.uuid = str(uuid.uuid4()) # to handle legacy files
        self.name = self.get_from_json(json_object, "name")
        self.card_type = self.get_from_json(json_object, "card_type")
        self.variant = self.get_from_json(json_object, "variant")
        if not self.variant in variant_names and not isfile("Template/Custom/" + self.card_type + "/" + self.variant):
            self.variant = "Classic"
        self.clan = self.get_from_json(json_object, "clan")
        if not self.clan in clan_names:
            self.clan = "None"
        self.minion_type = self.get_from_json(json_object, "minion_type")
        if not self.minion_type in minion_type_names:
            self.minion_type = "Action"
        filtered_disciplines = []
        for discipline_name in self.get_from_json(json_object, "discipline_icons"):
            if discipline_name in disciplines_names:
                filtered_disciplines.append(discipline_name)
        self.discipline_icons = filtered_disciplines
        if not self.discipline_icons:
            self.discipline_icons = []
        self.blood = self.get_from_json(json_object, "blood")
        self.pool = self.get_from_json(json_object, "pool")
        self.capacity = self.get_from_json(json_object, "capacity")
        self.advanced = self.get_from_json(json_object, "advanced")
        self.effect = self.get_from_json(json_object, "effect")
        self.art_box = self.get_from_json(json_object, "art_box")
        if not self.art_box:
            self.art_box = ()
        self.flavour = self.get_from_json(json_object, "flavour")
        self.artist = self.get_from_json(json_object, "artist")
        self.copyright = self.get_from_json(json_object, "copyright")
        self.block_number = self.get_from_json(json_object, "block_number")
        self.set_name = self.get_from_json(json_object, "set_name")
        self.set_number = self.get_from_json(json_object, "set_number")
        self.set_max_number = self.get_from_json(json_object, "set_max_number")
        self.border_color = tuple(self.get_from_json(json_object, "border_color"))
        if not self.border_color or len(self.border_color) != 4:
            self.border_color = (0, 0, 0, 255)
        self.border_sharp = self.get_from_json(json_object, "border_type")
        if not self.border_sharp:
            self.border_sharp = False
        self.locale_name = self.get_from_json(json_object, "locale_name")
        self.art_path = self.get_from_json(json_object, "art_path")
        if not self.art_path:
            self.art_path = "Template/Dummy.png"
        
    def toJson(self):  # Json object is indeed a dict
        json_object = {}
        json_object["uuid"] = self.uuid
        json_object["name"] = self.name
        json_object["card_type"] = self.card_type
        json_object["variant"] = self.variant
        json_object["clan"] = self.clan
        json_object["minion_type"] = self.minion_type
        json_object["discipline_icons"] = self.discipline_icons
        json_object["blood"] = self.blood
        json_object["pool"] = self.pool
        json_object["capacity"] = self.capacity
        json_object["advanced"] = self.advanced
        json_object["effect"] = self.effect
        json_object["art_box"] = self.art_box
        json_object["flavour"] = self.flavour
        json_object["artist"] = self.artist
        json_object["copyright"] = self.copyright
        json_object["block_number"] = self.block_number
        json_object["set_name"] = self.set_name
        json_object["set_number"] = self.set_number
        json_object["set_max_number"] = self.set_max_number
        json_object["border_color"] = self.border_color
        json_object["border_sharp"] = self.border_sharp
        json_object["locale_name"] = self.locale_name
        json_object["art_path"] = self.art_path

        return json_object

    def status(self):
        status = ""
        status += "Name: " + self.name + "\n"
        status += "Type: " + self.card_type + "\n"
        status += "Variant: " + str(self.variant) + "\n"
        status += "Clan: " + self.clan + "\n"
        status += "Minion Type: " + self.minion_type + "\n"
        val = ""
        for discipline_icon in self.discipline_icons:
            val += discipline_icon + " "
        status += "Class Icons: " + val + "\n"
        status += "Blood: " + self.blood + "\n"
        status += "Pool: " + self.pool + "\n"
        status += "Capacity: " + str(self.capacity) + "\n"
        status += "Advanced: " + self.advanced + "\n"
        status += "Effect: " + self.effect + "\n"
        status += "Flavour: " + str(self.flavour) + "\n"
        status += "Artist: " + self.artist + "\n"
        status += "Copyright: " + self.copyright + "\n"
        status += "Block number: " + str(self.block_number) + "\n"
        status += "Set Name: " + str(self.set_name) + "\n"
        status += "Set Number: " + str(self.set_number) + "\n"
        status += "Set Max Number: " + str(self.set_max_number) + "\n"
        status += "Border Color: " + self.border_color + "\n"
        status += "Border Type: " + self.border_type + "\n"

        return status


class WoWCardGenerator(object):
    def __init__(self):

        # Load default type names #
        locale_file = open("Locales/English.json")
        locale = json.load(locale_file)
        self.type_names = locale["type_names"]
        locale_file.close()

        # Load default font paths #
        self.main_font_path = "Template/Fonts/Latin/latin/Matrix-Bold.ttf"
        self.stat_font_path = "Template/Fonts/Latin/quorum_black_bt.ttf"
    #    self.effect_font_path = "Template/Fonts/Latin/GIL_.TTF"
    #    self.effect_bold_font_path = "Template/Fonts/Latin/GILB_0.TTF"
    #    self.effect_italic_font_path = "Template/Fonts/Latin/Trade Gothic LT Condensed No. 18 Oblique.ttf"
        self.effect_font_path = "Template/Fonts/Latin/Gill Sans Medium.otf"
        self.effect_bold_font_path = "Template/Fonts/Latin/Gill Sans Bold.otf"
        self.effect_italic_font_path = "Template/Fonts/Latin/Gill Sans Italic.otf"
        self.blood_font_path = "Template/Fonts/Latin/Gill Sans Medium.otf"
        self.cost_font_path = "Template/Fonts/Latin/Gill Sans Medium.otf"
        self.flavour_font_path = "Template/Fonts/Latin/Gill Sans Medium.otf"
        self.artist_copyright_font_path = "Template/Fonts/Latin/GillSans Condensed.otf"
        self.set_font_path = "Template/Fonts/Latin/Gill Sans Bold.otf"

        # Import template images #
        # ToDo? : pre-resize images #
        # Card borders #
        self.border_foreground = Image.open("Template/Border/Border Foreground.png")
        #self.border_sharp_foreground = Image.open("Template/Border/Border Sharp Foreground.png")
        self.border_background = Image.open("Template/Border/Border Background.png")
        self.border = Image.open("Template/Border/Border.png")
        self.border_sharp = Image.open("Template/Border/Border Sharp.png")

        # Vampire Clan frames #
        self.vampire_banu_haquim_frame = Image.open("Template/Vampire/BanuHaquimFrame.png").resize((692, 990), 1)
        self.vampire_brujah_frame = Image.open("Template/Vampire/BrujahFrame.png").resize((692, 990), 1)
        self.vampire_gangrel_frame = Image.open("Template/Vampire/GangrelFrame.png").resize((692, 990), 1)
        self.vampire_hecata_frame = Image.open("Template/Vampire/HecataFrame.png").resize((692, 990), 1)
        self.vampire_lasombra_frame = Image.open("Template/Vampire/LasombraFrame.png").resize((692, 990), 1)
        self.vampire_malkavian_frame = Image.open("Template/Vampire/MalkavianFrame.png").resize((692, 990), 1)
        self.vampire_ministry_frame = Image.open("Template/Vampire/MinistryFrame.png").resize((692, 990), 1)
        self.vampire_nosferatu_frame = Image.open("Template/Vampire/NosferatuFrame.png").resize((692, 990), 1)
        self.vampire_ravnos_frame = Image.open("Template/Vampire/RavnosFrame.png").resize((692, 990), 1)
        self.vampire_salubri_frame = Image.open("Template/Vampire/SalubriFrame.png").resize((692, 990), 1)
        self.vampire_toreador_frame = Image.open("Template/Vampire/ToreadorFrame.png").resize((692, 990), 1)
        self.vampire_tremere_frame = Image.open("Template/Vampire/TremereFrame.png").resize((692, 990), 1)
        self.vampire_tzimisce_frame = Image.open("Template/Vampire/TzimisceFrame.png").resize((692, 990), 1)
        self.vampire_ventrue_frame = Image.open("Template/Vampire/VentrueFrame.png").resize((692, 990), 1)
        self.vampire_clan_frame = Image.open("Template/Vampire/ClanFrame.png").resize((692, 990), 1)

        # Vampire Clan classic frames #
        self.vampire_alt_banu_haquim_frame = Image.open("Template/Classic Vampire/AltBanuHaquimFrame.png").resize((692, 990), 1)
        self.vampire_alt_brujah_frame = Image.open("Template/Classic Vampire/AltBrujahFrame.png").resize((692, 990), 1)
        self.vampire_alt_gangrel_frame = Image.open("Template/Classic Vampire/AltGangrelFrame.png").resize((692, 990), 1)
        self.vampire_alt_hecata_frame = Image.open("Template/Classic Vampire/AltHecataFrame.png").resize((692, 990), 1)
        self.vampire_alt_lasombra_frame = Image.open("Template/Classic Vampire/AltLasombraFrame.png").resize((692, 990), 1)
        self.vampire_alt_malkavian_frame = Image.open("Template/Classic Vampire/AltMalkavianFrame.png").resize((692, 990), 1)
        self.vampire_alt_ministry_frame = Image.open("Template/Classic Vampire/AltMinistryFrame.png").resize((692, 990), 1)
        self.vampire_alt_nosferatu_frame = Image.open("Template/Classic Vampire/AltNosferatuFrame.png").resize((692, 990), 1)
        self.vampire_alt_ravnos_frame = Image.open("Template/Classic Vampire/AltRavnosFrame.png").resize((692, 990), 1)
        self.vampire_alt_salubri_frame = Image.open("Template/Classic Vampire/AltSalubriFrame.png").resize((692, 990), 1)
        self.vampire_alt_toreador_frame = Image.open("Template/Classic Vampire/AltToreadorFrame.png").resize((692, 990), 1)
        self.vampire_alt_tremere_frame = Image.open("Template/Classic Vampire/AltTremereFrame.png").resize((692, 990), 1)
        self.vampire_alt_tzimisce_frame = Image.open("Template/Classic Vampire/AltTzimisceFrame.png").resize((692, 990), 1)
        self.vampire_alt_ventrue_frame = Image.open("Template/Classic Vampire/AltVentrueFrame.png").resize((692, 990), 1)
        self.vampire_alt_clan_frame = Image.open("Template/Classic Vampire/AltClanFrame.png").resize((692, 990), 1)

        # Vampire Clan old ugly frames #
        self.vampire_old_banu_haquim_frame = Image.open("Template/Old Vampire/OldBanuHaquimFrame.png").resize((692, 990), 1)
        self.vampire_old_brujah_frame = Image.open("Template/Old Vampire/OldBrujahFrame.png").resize((692, 990), 1)
        self.vampire_old_gangrel_frame = Image.open("Template/Old Vampire/OldGangrelFrame.png").resize((692, 990), 1)
        self.vampire_old_hecata_frame = Image.open("Template/Old Vampire/OldHecataFrame.png").resize((692, 990), 1)
        self.vampire_old_lasombra_frame = Image.open("Template/Old Vampire/OldLasombraFrame.png").resize((692, 990), 1)
        self.vampire_old_malkavian_frame = Image.open("Template/Old Vampire/OldMalkavianFrame.png").resize((692, 990), 1)
        self.vampire_old_ministry_frame = Image.open("Template/Old Vampire/OldMinistryFrame.png").resize((692, 990), 1)
        self.vampire_old_nosferatu_frame = Image.open("Template/Old Vampire/OldNosferatuFrame.png").resize((692, 990), 1)
        self.vampire_old_ravnos_frame = Image.open("Template/Old Vampire/OldRavnosFrame.png").resize((692, 990), 1)
        self.vampire_old_salubri_frame = Image.open("Template/Old Vampire/OldSalubriFrame.png").resize((692, 990), 1)
        self.vampire_old_toreador_frame = Image.open("Template/Old Vampire/OldToreadorFrame.png").resize((692, 990), 1)
        self.vampire_old_tremere_frame = Image.open("Template/Old Vampire/OldTremereFrame.png").resize((692, 990), 1)
        self.vampire_old_tzimisce_frame = Image.open("Template/Old Vampire/OldTzimisceFrame.png").resize((692, 990), 1)
        self.vampire_old_ventrue_frame = Image.open("Template/Old Vampire/OldVentrueFrame.png").resize((692, 990), 1)
        self.vampire_old_clan_frame = Image.open("Template/Old Vampire/OldClanFrame.png").resize((692, 990), 1)

        # Minion card type frames #
        self.minion_action_frame = Image.open("Template/Minion/ActionFrame.png").resize((692, 990), 1)
        self.minion_reaction_frame = Image.open("Template/Minion/ReactionFrame.png").resize((692, 990), 1)
        self.minion_ally_frame = Image.open("Template/Minion/AllyFrame.png").resize((692, 990), 1)
        self.minion_modifier_ally_frame = Image.open("Template/Minion/ModifierAllyFrame.png").resize((692, 990), 1)
        self.minion_retainer_frame = Image.open("Template/Minion/RetainerFrame.png").resize((692, 990), 1)
        self.minion_combat_retainer_frame = Image.open("Template/Minion/CombatRetainerFrame.png").resize((692, 990), 1)
        self.minion_reaction_retainer_frame = Image.open("Template/Minion/ReactionRetainerFrame.png").resize((692, 990), 1)
        self.minion_equipment_frame = Image.open("Template/Minion/EquipmentFrame.png").resize((692, 990), 1)
        self.minion_combat_equipment_frame = Image.open("Template/Minion/CombatEquipmentFrame.png").resize((692, 990), 1)
        self.minion_political_frame = Image.open("Template/Minion/PoliticalFrame.png").resize((692, 990), 1)
        self.minion_combat_frame = Image.open("Template/Minion/CombatFrame.png").resize((692, 990), 1)
        self.minion_modifier_frame = Image.open("Template/Minion/ModifierFrame.png").resize((692, 990), 1)
        self.minion_modifier_reaction_frame = Image.open("Template/Minion/ModifierReactionFrame.png").resize((692, 990), 1)
        self.minion_modifier_combat_frame = Image.open("Template/Minion/ModifierCombatFrame.png").resize((692, 990), 1)
        self.minion_action_combat_frame = Image.open("Template/Minion/ActionCombatFrame.png").resize((692, 990), 1)
        self.minion_action_reaction_frame = Image.open("Template/Minion/ActionReactionFrame.png").resize((692, 990), 1)
        self.minion_combat_reaction_frame = Image.open("Template/Minion/CombatReactionFrame.png").resize((692, 990), 1)
        self.minion_master_frame = Image.open("Template/Minion/MasterFrame.png").resize((692, 990), 1)
        self.minion_event_frame = Image.open("Template/Minion/EventFrame.png").resize((692, 990), 1)

        # Minion card type classic frames #
        self.minion_alt_action_frame = Image.open("Template/Classic Minion/AltActionFrame.png").resize((692, 990), 1)
        self.minion_alt_political_frame = Image.open("Template/Classic Minion/AltPoliticalFrame.png").resize((692, 990), 1)
        self.minion_alt_ally_frame = Image.open("Template/Classic Minion/AltAllyFrame.png").resize((692, 990), 1)
        self.minion_alt_modifier_ally_frame = Image.open("Template/Classic Minion/AltModifierAllyFrame.png").resize((692, 990), 1)
        self.minion_alt_retainer_frame = Image.open("Template/Classic Minion/AltRetainerFrame.png").resize((692, 990), 1)
        self.minion_alt_combat_retainer_frame = Image.open("Template/Classic Minion/AltCombatRetainerFrame.png").resize((692, 990), 1)
        self.minion_alt_reaction_retainer_frame = Image.open("Template/Classic Minion/AltReactionRetainerFrame.png").resize((692, 990), 1)
        self.minion_alt_equipment_frame = Image.open("Template/Classic Minion/AltEquipmentFrame.png").resize((692, 990), 1)
        self.minion_alt_combat_equipment_frame = Image.open("Template/Classic Minion/AltCombatEquipmentFrame.png").resize((692, 990), 1)
        self.minion_alt_modifier_frame = Image.open("Template/Classic Minion/AltModifierFrame.png").resize((692, 990), 1)
        self.minion_alt_combat_frame = Image.open("Template/Classic Minion/AltCombatFrame.png").resize((692, 990), 1)
        self.minion_alt_reaction_frame = Image.open("Template/Classic Minion/AltReactionFrame.png").resize((692, 990), 1)
        self.minion_alt_modifier_reaction_frame = Image.open("Template/Classic Minion/AltModifierReactionFrame.png").resize((692, 990), 1)
        self.minion_alt_modifier_combat_frame = Image.open("Template/Classic Minion/AltModifierCombatFrame.png").resize((692, 990), 1)
        self.minion_alt_action_combat_frame = Image.open("Template/Classic Minion/AltActionCombatFrame.png").resize((692, 990), 1)
        self.minion_alt_action_reaction_frame = Image.open("Template/Classic Minion/AltActionReactionFrame.png").resize((692, 990), 1)
        self.minion_alt_combat_reaction_frame = Image.open("Template/Classic Minion/AltCombatReactionFrame.png").resize((692, 990), 1)
        self.minion_alt_master_frame = Image.open("Template/Classic Minion/AltMasterFrame.png").resize((692, 990), 1)
        self.minion_alt_event_frame = Image.open("Template/Classic Minion/AltEventFrame.png").resize((692, 990), 1)
        # Minion card type icons #
        self.action_icon = Image.open("Template/Minion/Action.png").resize((64,64), 1)
        self.reaction_icon = Image.open("Template/Minion/Reaction.png").resize((64,64), 1)
        self.modifier_icon = Image.open("Template/Minion/Modifier.png").resize((64,64), 1)
        self.combat_icon = Image.open("Template/Minion/Combat.png").resize((64,64), 1)
        self.ally_icon = Image.open("Template/Minion/Ally.png").resize((64,64), 1)
        self.political_icon = Image.open("Template/Minion/Political.png").resize((64,64), 1)
        self.equipment_icon = Image.open("Template/Minion/Equipment.png").resize((64,64), 1)
        self.retainer_icon = Image.open("Template/Minion/Retainer.png").resize((64,64), 1)
        self.event_icon = Image.open("Template/Minion/Event.png").resize((64, 96), 1)
        self.reflex_icon = Image.open("Template/Minion/Reflex.png").resize((64,64), 1)

        # Minion card type text #
        self.action_text = Image.open("Template/Minion/ActionText.png").resize((70, 20), 1)
        self.modifier_text = Image.open("Template/Minion/ModifierText.png").resize((80, 20), 1)
        self.combat_text = Image.open("Template/Minion/CombatText.png").resize((70, 20), 1)
        self.reaction_text = Image.open("Template/Minion/ReactionText.png").resize((80, 20), 1)
        self.employ_text = Image.open("Template/Minion/EmployText.png").resize((70, 20), 1)
        self.recruit_text = Image.open("Template/Minion/RecruitText.png").resize((70, 20), 1)
        self.equip_text = Image.open("Template/Minion/EquipText.png").resize((70, 20), 1)
        self.political_text = Image.open("Template/Minion/PoliticalText.png").resize((80, 20), 1)
        self.vote_text = Image.open("Template/Minion/1VoteText.png").resize((70, 20), 1)
        self.event_text = Image.open("Template/Minion/EventText.png").resize((70, 20), 1)
        self.master_text = Image.open("Template/Minion/MasterText.png").resize((70, 20), 1)

        # Utility stuff #
        self.advanced_icon = Image.open("Template/Vampire/Advanced.png").resize((30, 60), 1)
        self.capacity_icon = Image.open("Template/Vampire/Capacity.png").resize((64, 64), 1)
        self.blood_icon = Image.open("Template/Minion/Blood.png").resize((51, 96), 1)
        self.pool_icon = Image.open("Template/Minion/Pool.png").resize((84, 162), 1)
        self.alt_pool_icon = Image.open("Template/Minion/AltPool.png").resize((58, 108), 1)
        self.textbox_frame = Image.open("Template/Vampire/Textbox.png").resize((650, 150), 1)
        self.textbox_frame_old = Image.open("Template/Vampire/TextboxOld.png").resize((650, 150), 1)
        self.alt_textbox_frame = Image.open("Template/Vampire/AltTextbox.png").resize((550, 150), 1)

        # Clan icons #
        self.banu_haquim_icon = Image.open("Template/Vampire/BanuHaquimIcon.png").resize((90, 90), 1)
        self.brujah_icon = Image.open("Template/Vampire/BrujahIcon.png").resize((90, 90), 1)
        self.gangrel_icon = Image.open("Template/Vampire/GangrelIcon.png").resize((95, 84), 1)
        self.hecata_icon = Image.open("Template/Vampire/HecataIcon.png").resize((99, 88), 1)
        self.lasombra_icon = Image.open("Template/Vampire/LasombraIcon.png").resize((77, 66), 1)
        self.malkavian_icon = Image.open("Template/Vampire/MalkavianIcon.png").resize((56, 110), 1)
        self.ministry_icon = Image.open("Template/Vampire/MinistryIcon.png").resize((110, 56), 1)
        self.nosferatu_icon = Image.open("Template/Vampire/NosferatuIcon.png").resize((67, 100), 1)
        self.ravnos_icon = Image.open("Template/Vampire/RavnosIcon.png").resize((90, 90), 1)
        self.salubri_icon = Image.open("Template/Vampire/SalubriIcon.png").resize((85, 85), 1)
        self.toreador_icon = Image.open("Template/Vampire/ToreadorIcon.png").resize((90, 90), 1)
        self.tremere_icon = Image.open("Template/Vampire/TremereIcon.png").resize((90, 90), 1)
        self.tzimisce_icon = Image.open("Template/Vampire/TzimisceIcon.png").resize((77, 77), 1)
        self.ventrue_icon = Image.open("Template/Vampire/VentrueIcon.png").resize((90, 90), 1)

        # Superior Disciplines #
        self.superior_visceratika_icon = Image.open("Template/Discipline Icons/SuperiorVisceratika.png").resize((70, 70), 1)
        self.superior_vicissitude_icon = Image.open("Template/Discipline Icons/SuperiorVicissitude.png").resize((70, 70), 1)
        self.superior_valeren_icon = Image.open("Template/Discipline Icons/SuperiorValeren.png").resize((70, 70), 1)
        self.superior_thanatosis_icon = Image.open("Template/Discipline Icons/SuperiorThanatosis.png").resize((70, 70), 1)
        self.superior_thaumaturgy_icon = Image.open("Template/Discipline Icons/SuperiorThaumaturgy.png").resize((70, 70), 1)
        self.superior_temporis_icon = Image.open("Template/Discipline Icons/SuperiorTemporis.png").resize((70, 70), 1)
        self.superior_spiritus_icon = Image.open("Template/Discipline Icons/SuperiorSpiritus.png").resize((70, 70), 1)
        self.superior_serpentis_icon = Image.open("Template/Discipline Icons/SuperiorSerpentis.png").resize((70, 70), 1)
        self.superior_sanguinus_icon = Image.open("Template/Discipline Icons/SuperiorSanguinus.png").resize((70, 70), 1)
        self.superior_quietus_icon = Image.open("Template/Discipline Icons/SuperiorQuietus.png").resize((70, 70), 1)
        self.superior_protean_icon = Image.open("Template/Discipline Icons/SuperiorProtean.png").resize((70, 70), 1)
        self.superior_presence_icon = Image.open("Template/Discipline Icons/SuperiorPresence.png").resize((72, 72), 1)
        self.superior_potence_icon = Image.open("Template/Discipline Icons/SuperiorPotence.png").resize((70, 70), 1)
        self.superior_obtenebration_icon = Image.open("Template/Discipline Icons/SuperiorObtenebration.png").resize((70, 70), 1)
        self.superior_oblivion_icon = Image.open("Template/Discipline Icons/SuperiorOblivion.png").resize((70, 70), 1)
        self.superior_obfuscate_icon = Image.open("Template/Discipline Icons/SuperiorObfuscate.png").resize((70, 70), 1)
        self.superior_obeah_icon = Image.open("Template/Discipline Icons/SuperiorObeah.png").resize((70, 70), 1)
        self.superior_necromancy_icon = Image.open("Template/Discipline Icons/SuperiorNecromancy.png").resize((70, 70), 1)
        self.superior_mytherceria_icon = Image.open("Template/Discipline Icons/SuperiorMytherceria.png").resize((70, 70), 1)
        self.superior_melopomine_icon = Image.open("Template/Discipline Icons/SuperiorMelopomine.png").resize((70, 70), 1)
        self.superior_fortitude_icon = Image.open("Template/Discipline Icons/SuperiorFortitude.png").resize((70, 70), 1)
        self.superior_dominate_icon = Image.open("Template/Discipline Icons/SuperiorDominate.png").resize((70, 70), 1)
        self.superior_dementation_icon = Image.open("Template/Discipline Icons/SuperiorDementation.png").resize((70, 70), 1)
        self.superior_daimonion_icon = Image.open("Template/Discipline Icons/SuperiorDaimonion.png").resize((70, 70), 1)
        self.superior_chimerstry_icon = Image.open("Template/Discipline Icons/SuperiorChimerstry.png").resize((70, 70), 1)
        self.superior_celerity_icon = Image.open("Template/Discipline Icons/SuperiorCelerity.png").resize((70, 70), 1)
        self.superior_blood_sorcery_icon = Image.open("Template/Discipline Icons/SuperiorBloodSorcery.png").resize((70, 70), 1)
        self.superior_auspex_icon = Image.open("Template/Discipline Icons/SuperiorAuspex.png").resize((70, 70), 1)
        self.superior_animalism_icon = Image.open("Template/Discipline Icons/SuperiorAnimalism.png").resize((70, 70), 1)
        self.superior_abombwe_icon = Image.open("Template/Discipline Icons/SuperiorAbombwe.png").resize((70, 70), 1)
        # Inferior Disciplines #
        self.inferior_visceratika_icon = Image.open("Template/Discipline Icons/InferiorVisceratika.png").resize((56,56), 1)
        self.inferior_vicissitude_icon = Image.open("Template/Discipline Icons/InferiorVicissitude.png").resize((56,56), 1)
        self.inferior_valeren_icon = Image.open("Template/Discipline Icons/InferiorValeren.png").resize((56,56), 1)
        self.inferior_thaumaturgy_icon = Image.open("Template/Discipline Icons/InferiorThaumaturgy.png").resize((56,56), 1)
        self.inferior_thanatosis_icon = Image.open("Template/Discipline Icons/InferiorThanatosis.png").resize((56,56), 1)
        self.inferior_temporis_icon = Image.open("Template/Discipline Icons/InferiorTemporis.png").resize((56,56), 1)
        self.inferior_spiritus_icon = Image.open("Template/Discipline Icons/InferiorSpiritus.png").resize((56,56), 1)
        self.inferior_serpentis_icon = Image.open("Template/Discipline Icons/InferiorSerpentis.png").resize((56,56), 1)
        self.inferior_sanguinus_icon = Image.open("Template/Discipline Icons/InferiorSanguinus.png").resize((56,56), 1)
        self.inferior_quietus_icon = Image.open("Template/Discipline Icons/InferiorQuietus.png").resize((56,56), 1)
        self.inferior_protean_icon = Image.open("Template/Discipline Icons/InferiorProtean.png").resize((56,56), 1)
        self.inferior_presence_icon = Image.open("Template/Discipline Icons/InferiorPresence.png").resize((56,56), 1)
        self.inferior_potence_icon = Image.open("Template/Discipline Icons/InferiorPotence.png").resize((56,56), 1)
        self.inferior_obtenebration_icon = Image.open("Template/Discipline Icons/InferiorObtenebration.png").resize((56,56), 1)
        self.inferior_oblivion_icon = Image.open("Template/Discipline Icons/InferiorOblivion.png").resize((56,56), 1)
        self.inferior_obfuscate_icon = Image.open("Template/Discipline Icons/InferiorObfuscate.png").resize((56,56), 1)
        self.inferior_obeah_icon = Image.open("Template/Discipline Icons/InferiorObeah.png").resize((56,56), 1)
        self.inferior_necromancy_icon = Image.open("Template/Discipline Icons/InferiorNecromancy.png").resize((56,56), 1)
        self.inferior_mytherceria_icon = Image.open("Template/Discipline Icons/InferiorMytherceria.png").resize((56,56), 1)
        self.inferior_melopomine_icon = Image.open("Template/Discipline Icons/InferiorMelopomine.png").resize((56,56), 1)
        self.inferior_fortitude_icon = Image.open("Template/Discipline Icons/InferiorFortitude.png").resize((56,56), 1)
        self.inferior_dominate_icon = Image.open("Template/Discipline Icons/InferiorDominate.png").resize((56,56), 1)
        self.inferior_dementation_icon = Image.open("Template/Discipline Icons/InferiorDementation.png").resize((56,56), 1)
        self.inferior_daimonion_icon = Image.open("Template/Discipline Icons/InferiorDaimonion.png").resize((56,56), 1)
        self.inferior_chimerstry_icon = Image.open("Template/Discipline Icons/InferiorChimerstry.png").resize((56,56), 1)
        self.inferior_celerity_icon = Image.open("Template/Discipline Icons/InferiorCelerity.png").resize((56,56), 1)
        self.inferior_blood_sorcery_icon = Image.open("Template/Discipline Icons/InferiorBloodSorcery.png").resize((56,56), 1)
        self.inferior_auspex_icon = Image.open("Template/Discipline Icons/InferiorAuspex.png").resize((56,56), 1)
        self.inferior_animalism_icon = Image.open("Template/Discipline Icons/InferiorAnimalism.png").resize((56,56), 1)
        self.inferior_abombwe_icon = Image.open("Template/Discipline Icons/InferiorAbombwe.png").resize((56,56), 1)
        self.flight_icon = Image.open("Template/Discipline Icons/Flight.png").resize((64,44), 1)


    def createCard(self, card, bleeding=[752, 1050]):
        # Load locale (defaults to English) #
        if not card.locale_name or not exists("Locales/" + card.locale_name + ".json"):
            card.locale_name = "English"

        locale_file = open("Locales/" + card.locale_name + ".json", encoding="utf-8")
        card.locale = json.load(locale_file)
        locale_file.close()

        # Switch fonts if necessary #
        if card.locale_name == "Chinese":
            self.main_font_path = "Template/Fonts/Chinese/Simli.ttf"
            self.effect_font_path = "Template/Fonts/Chinese/Simhei.ttf"
            self.effect_bold_font_path = "Template/Fonts/Chinese/Simhei-Bold.ttf"
            self.effect_italic_font_path = "Template/Fonts/Chinese/Simkai.ttf"
            self.flavour_font_path = "Template/Fonts/Chinese/Simkai.ttf"

        if card.locale_name == "Russian":
            self.main_font_path = "Template/Fonts/Cyrillic/belwebdbtrusbyme_bold.otf"
            self.effect_font_path = "Template/Fonts/Cyrillic/Sign.ttf"
            self.effect_bold_font_path = "Template/Fonts/Cyrillic/Manrope Bold.ttf"
            self.effect_italic_font_path = "Template/Fonts/Cyrillic/Mysl Italic.ttf"
            self.flavour_font_path = "Template/Fonts/Cyrillic/Piazzolla Italic.ttf"

        else:
            self.main_font_path = "Template/Fonts/Latin/Matrix-Bold.ttf"
    #    self.effect_font_path = "Template/Fonts/Latin/GIL_.TTF"
    #    self.effect_bold_font_path = "Template/Fonts/Latin/GILB_0.TTF"
    #    self.effect_italic_font_path = "Template/Fonts/Latin/Trade Gothic LT Condensed No. 18 Oblique.ttf"
            self.effect_font_path = "Template/Fonts/Latin/Gill Sans Medium.otf"
            self.effect_bold_font_path = "Template/Fonts/Latin/Gill Sans Bold.otf"
            self.effect_italic_font_path = "Template/Fonts/Latin/Gill Sans Italic.otf"
            self.flavour_font_path = "Template/Fonts/Latin/Gill Sans Italic.otf"



        # Prepare card layer #
        card.image = Image.new("RGBA", (752, 1050), 0)
        # Retrieve a draw handle to write text #
        card.draw = ImageDraw.Draw(card.image)
        # Draw card #
        card.image.paste(self.border_background)

        # Draw art #
        if not card.art:
            try:
                art = Image.open(card.art_path)
            except Exception:
                print("Could not find art file")
                art = Image.open("Template/Dummy.png")
                card.art_path = "Template/Dummy.png"
        else:
            art = card.art
        if card.art_box:
            if len(card.art_box) != 4:
                # Fix for resize function #
                card.art_box = None
        else:
            # Fix for resize function #
            card.art_box = None

        # Change - to — #
        card.flavour = card.flavour.replace("--", "—")
        card_type = card.card_type

        if card.variant in "Classic" in card.variant and card.card_type == "Minion" in card.card_type:
                # Resize to more or less 1.08 width / height ratio (better have a smaller ratio to avoid white space) #
                art = art.resize((572, 495), 1, card.art_box)
                card.image.paste(art, (150, 100))
        elif card.variant in "Classic" in card.variant and card.card_type == "Vampire" in card.card_type:
                art = art.resize((542, 710), 1, card.art_box)
                card.image.paste(art, (155, 110))
        elif card.variant in "Old" in card.variant and card.card_type == "Vampire" in card.card_type:
                art = art.resize((550, 710), 1, card.art_box)
                card.image.paste(art, (102, 105))
        else:
            # Resize to full card ratio #
            art = art.resize((692, 990), 1, card.art_box)
            card.image.paste(art, (30, 30))

        if card_type == "Vampire":
            self.drawVampire(card)
        elif card_type == "Minion":
            self.drawMinion(card)

        # Draw artist and copyright #
        
        if card.artist:
            self.drawArtist(card)
        if card.copyright:
            self.drawCopyright(card)
            # Set icon and text #
            self.drawSet(card)

        # Card border #
        if card.border_sharp:
            _, _, _, alpha = self.border_sharp.split()
            gray = ImageOps.grayscale(self.border_sharp)
            result = ImageOps.colorize(gray, card.border_color, (0, 0, 0, 0)) 
            result.putalpha(alpha)
        else:
            _, _, _, alpha = self.border.split()
            gray = ImageOps.grayscale(self.border)
            result = ImageOps.colorize(gray, card.border_color, (0, 0, 0, 0)) 
            result.putalpha(alpha)

        card.image.alpha_composite(result)
        card.image.alpha_composite(self.border_foreground)

        return card.image

    def drawName(self, card, font_size, min_width, max_width, height):
        font = font_size
        font_size_computed = False
        name_box_width = max_width - min_width

        while not font_size_computed:
            card_name_font = ImageFont.truetype(self.main_font_path, font)
            _, _ , width, _ = card.draw.textbbox((0,0),card.name, font=card_name_font)

            if width <= name_box_width:
                font_size_computed = True
            else:
                font -= 1

        card.draw.text((min_width, height + (font_size - font) / 2), card.name, font=card_name_font, fill=(255, 255, 255, 255), stroke_width=1, stroke_fill=(0, 0, 0, 255), spacing=6
        )

    def drawBlood(self, card, font_size, width, height):
        if card.variant == "FullBleed" or exists("Template/Custom/Minion/" + card.variant):
            height -= 280
        if card.blood:
            card.image.alpha_composite(self.blood_icon, (width-26, height-38))
            blood_font = ImageFont.truetype(self.stat_font_path, font_size)
            x_box = card.draw.textbbox((0,0),card.blood, font=blood_font)
            x_offset = (x_box[2] - x_box [0])/2
            card.draw.text((width - x_offset, height), card.blood, font=blood_font, fill=(255, 255, 255, 255), stroke_width=0, stroke_fill=(0, 0, 0, 255))

    def drawPool(self, card, font_size, width, height):
        if card.variant == "FullBleed" or exists("Template/Custom/Minion/" + card.variant):
            height -= 302
            font_size = 40
        if card.pool:
            if card.variant == "FullBleed" or exists("Template/Custom/Minion/" + card.variant):
                card.image.alpha_composite(self.alt_pool_icon, (width-29, height-38))
            else:
                card.image.alpha_composite(self.pool_icon, (width-42, height-88))
            blood_font = ImageFont.truetype(self.stat_font_path, font_size)
            x_box = card.draw.textbbox((0,0),card.pool, font=blood_font)
            x_offset = (x_box[2] - x_box [0])/2
            card.draw.text((width - x_offset, height), card.pool, font=blood_font, fill=(0, 0, 0, 255), stroke_width=2, stroke_fill=(255, 255, 255, 255))


    def drawCapacity(self, card, font_size, width, height):
        if card.capacity:
            if card.variant in ["FullBleed", "Alternate"] or exists("Template/Custom/Vampire/" + card.variant) or exists("Template/Custom/Alternate Vampire/" + card.variant) :
                card.image.alpha_composite(self.capacity_icon, (width-32, height-4))
            if card.variant in ["Old"] or exists("Template/Custom/Old Vampire/" + card.variant):
                card.image.alpha_composite(self.capacity_icon.resize((78, 78),1), (width-39, height-7))
            elif card.card_type == "Minion" or exists("Template/Custom/Minion/" + card.variant) or exists("Template/Custom/Alternate Minion/" + card.variant) or exists("Template/Custom/Classic Minion/" + card.variant):
                card.image.alpha_composite(self.capacity_icon, (width-32, height-4))
            hp_font = ImageFont.truetype(self.stat_font_path, font_size)

            x_box = card.draw.textbbox((0,0),card.capacity, font=hp_font)
            x_offset = (x_box[2] - x_box [0])/2
            card.draw.text((width - x_offset, height), card.capacity, font=hp_font, fill=(255, 255, 255, 255), stroke_width=0, stroke_fill=(0, 0, 0, 255))

    def drawAdvanced(self, card, width=67, height=239):
        if card.advanced == True:
            card.image.alpha_composite(self.advanced_icon, (width, height))

    def drawMinionType(self, card, width, height):
        minion_type = card.minion_type

        if minion_type and card.card_type == "Minion":
            if card.variant not in ["FullBleed", "Alternate", "Classic", "Old"] and exists("Template/Custom/Classic Minion/" + card.variant):
                    card.image.alpha_composite(Image.open("Template/Custom/Classic Minion/" + card.variant).resize((692, 990), 1), (30, 30))
                 #   card.image.alpha_composite(self.textbox_frame, (50, 830))
            elif minion_type == "Action":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_action_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.action_text, (48, 205))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_action_frame, (30, 30))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.action_text, (48, 205))
                else:
                    card.image.alpha_composite(self.minion_action_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.action_text, (48, 205))
            elif minion_type == "Ally":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_ally_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.ally_icon, (51, 212))
                    card.image.alpha_composite(self.recruit_text, (48, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_ally_frame, (30, 30))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.ally_icon, (51, 212))
                    card.image.alpha_composite(self.recruit_text, (48, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
                else:
                    card.image.alpha_composite(self.minion_ally_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.ally_icon, (51, 212))
                    card.image.alpha_composite(self.recruit_text, (48, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
            elif minion_type == "ModifierAlly":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_modifier_ally_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.modifier_icon, (51, 138))
                    card.image.alpha_composite(self.ally_icon, (51, 212))
                    card.image.alpha_composite(self.recruit_text, (48, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_modifier_ally_frame, (30, 30))
                    card.image.alpha_composite(self.modifier_icon, (51, 138))
                    card.image.alpha_composite(self.ally_icon, (51, 212))
                    card.image.alpha_composite(self.recruit_text, (48, 279))
                    card.image.alpha_composite(self.modifier_text, (48, 299))
                else:
                    card.image.alpha_composite(self.minion_modifier_ally_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.modifier_icon, (51, 138))
                    card.image.alpha_composite(self.ally_icon, (51, 212))
                    card.image.alpha_composite(self.recruit_text, (48, 279))
                    card.image.alpha_composite(self.modifier_text, (48, 299))
            elif minion_type == "Retainer":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_retainer_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.retainer_icon, (51, 212))
                    card.image.alpha_composite(self.employ_text, (48, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_retainer_frame, (30, 30))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.retainer_icon, (51, 212))
                    card.image.alpha_composite(self.employ_text, (48, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
                else:
                    card.image.alpha_composite(self.minion_retainer_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.retainer_icon, (51, 212))
                    card.image.alpha_composite(self.employ_text, (48, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
            elif minion_type == "CombatRetainer":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_combat_retainer_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.combat_icon, (51, 138))
                    card.image.alpha_composite(self.retainer_icon, (51, 212))
                    card.image.alpha_composite(self.employ_text, (48, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_combat_retainer_frame, (30, 30))
                    card.image.alpha_composite(self.combat_icon, (51, 138))
                    card.image.alpha_composite(self.retainer_icon, (51, 212))
                    card.image.alpha_composite(self.employ_text, (48, 279))
                    card.image.alpha_composite(self.combat_text, (48, 299))
                else:
                    card.image.alpha_composite(self.minion_combat_retainer_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.combat_icon, (51, 138))
                    card.image.alpha_composite(self.retainer_icon, (51, 212))
                    card.image.alpha_composite(self.employ_text, (48, 279))
                    card.image.alpha_composite(self.combat_text, (48, 299))
            elif minion_type == "ReactionRetainer":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_reaction_retainer_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.reaction_icon, (51, 138))
                    card.image.alpha_composite(self.retainer_icon, (51, 212))
                    card.image.alpha_composite(self.employ_text, (48, 279))
                    card.image.alpha_composite(self.reaction_text, (48, 299))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_reaction_retainer_frame, (30, 30))
                    card.image.alpha_composite(self.reaction_icon, (51, 138))
                    card.image.alpha_composite(self.retainer_icon, (51, 212))
                    card.image.alpha_composite(self.employ_text, (48, 279))
                    card.image.alpha_composite(self.reaction_text, (48, 299))
                else:
                    card.image.alpha_composite(self.minion_reaction_retainer_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.reaction_icon, (51, 138))
                    card.image.alpha_composite(self.retainer_icon, (51, 212))
                    card.image.alpha_composite(self.employ_text, (48, 279))
                    card.image.alpha_composite(self.reaction_text, (48, 299))
            elif minion_type == "Equipment":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_equipment_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.equipment_icon, (51, 212))
                    card.image.alpha_composite(self.equip_text, (48, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_equipment_frame, (30, 30))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.equipment_icon, (51, 212))
                    card.image.alpha_composite(self.equip_text, (48, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
                else:
                    card.image.alpha_composite(self.minion_equipment_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.equipment_icon, (51, 212))
                    card.image.alpha_composite(self.equip_text, (48, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
            elif minion_type == "CombatEquipment":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_combat_equipment_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.combat_icon, (51, 138))
                    card.image.alpha_composite(self.equipment_icon, (51, 212))
                    card.image.alpha_composite(self.equip_text, (48, 279))
                    card.image.alpha_composite(self.combat_text, (48, 299))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_combat_equipment_frame, (30, 30))
                    card.image.alpha_composite(self.combat_icon, (51, 138))
                    card.image.alpha_composite(self.equipment_icon, (51, 212))
                    card.image.alpha_composite(self.equip_text, (48, 279))
                    card.image.alpha_composite(self.combat_text, (48, 299))
                else:
                    card.image.alpha_composite(self.minion_combat_equipment_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.combat_icon, (51, 138))
                    card.image.alpha_composite(self.equipment_icon, (51, 212))
                    card.image.alpha_composite(self.equip_text, (48, 279))
                    card.image.alpha_composite(self.combat_text, (48, 299))
            elif minion_type == "Political":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_political_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.political_icon, (51, 212))
                    card.image.alpha_composite(self.political_text, (43, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
                    card.image.alpha_composite(self.vote_text, (48, 319))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_political_frame, (30, 30))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.political_icon, (51, 212))
                    card.image.alpha_composite(self.political_text, (43, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
                    card.image.alpha_composite(self.vote_text, (48, 319))
                else:
                    card.image.alpha_composite(self.minion_political_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.political_icon, (51, 212))
                    card.image.alpha_composite(self.political_text, (43, 279))
                    card.image.alpha_composite(self.action_text, (48, 299))
                    card.image.alpha_composite(self.vote_text, (48, 319))
            elif minion_type == "Combat":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_combat_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.combat_icon, (51, 138))
                    card.image.alpha_composite(self.combat_text, (48, 205))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_combat_frame, (30, 30))
                    card.image.alpha_composite(self.combat_icon, (51, 138))
                    card.image.alpha_composite(self.combat_text, (48, 205))
                else:
                    card.image.alpha_composite(self.minion_combat_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.combat_icon, (51, 138))
                    card.image.alpha_composite(self.combat_text, (48, 205))
            elif minion_type == "Reaction":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_reaction_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.reaction_icon, (51, 138))
                    card.image.alpha_composite(self.reaction_text, (43, 205))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_reaction_frame, (30, 30))
                    card.image.alpha_composite(self.reaction_icon, (51, 138))
                    card.image.alpha_composite(self.reaction_text, (43, 205))
                else:
                    card.image.alpha_composite(self.minion_reaction_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.reaction_icon, (51, 138))
                    card.image.alpha_composite(self.reaction_text, (43, 205))
            elif minion_type == "Modifier":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_modifier_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.modifier_icon, (51, 138))
                    card.image.alpha_composite(self.modifier_text, (43, 205))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_modifier_frame, (30, 30))
                    card.image.alpha_composite(self.modifier_icon, (51, 138))
                    card.image.alpha_composite(self.modifier_text, (43, 205))
                else:
                    card.image.alpha_composite(self.minion_modifier_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.modifier_icon, (51, 138))
                    card.image.alpha_composite(self.modifier_text, (43, 205))
            elif minion_type == "ActionReaction":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_action_reaction_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.reaction_icon, (51, 228))
                    card.image.alpha_composite(self.action_text, (48, 205))
                    card.image.alpha_composite(self.reaction_text, (43, 295))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_action_reaction_frame, (30, 30))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.reaction_icon, (51, 228))
                    card.image.alpha_composite(self.action_text, (48, 205))
                    card.image.alpha_composite(self.reaction_text, (43, 295))
                else:
                    card.image.alpha_composite(self.minion_action_reaction_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.reaction_icon, (51, 228))
                    card.image.alpha_composite(self.action_text, (48, 205))
                    card.image.alpha_composite(self.reaction_text, (43, 295))
            elif minion_type == "ActionCombat":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_action_combat_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.combat_icon, (51, 228))
                    card.image.alpha_composite(self.action_text, (48, 205))
                    card.image.alpha_composite(self.combat_text, (48, 295))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_action_combat_frame, (30, 30))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.combat_icon, (51, 228))
                    card.image.alpha_composite(self.action_text, (48, 205))
                    card.image.alpha_composite(self.combat_text, (48, 295))
                else:
                    card.image.alpha_composite(self.minion_action_combat_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.action_icon, (51, 138))
                    card.image.alpha_composite(self.combat_icon, (51, 228))
                    card.image.alpha_composite(self.action_text, (48, 205))
                    card.image.alpha_composite(self.combat_text, (48, 295))
            elif minion_type == "ModifierCombat":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_modifier_combat_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.modifier_icon, (51, 138))
                    card.image.alpha_composite(self.combat_icon, (51, 228))
                    card.image.alpha_composite(self.modifier_text, (43, 205))
                    card.image.alpha_composite(self.combat_text, (48, 295))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_modifier_combat_frame, (30, 30))
                    card.image.alpha_composite(self.modifier_icon, (51, 138))
                    card.image.alpha_composite(self.combat_icon, (51, 228))
                    card.image.alpha_composite(self.modifier_text, (43, 205))
                    card.image.alpha_composite(self.combat_text, (48, 295))
                else:
                    card.image.alpha_composite(self.minion_modifier_combat_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.modifier_icon, (51, 138))
                    card.image.alpha_composite(self.combat_icon, (51, 228))
                    card.image.alpha_composite(self.modifier_text, (43, 205))
                    card.image.alpha_composite(self.combat_text, (48, 295))
            elif minion_type == "ModifierReaction":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_modifier_reaction_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.modifier_icon, (51, 138))
                    card.image.alpha_composite(self.reaction_icon, (51, 228))
                    card.image.alpha_composite(self.modifier_text, (43, 205))
                    card.image.alpha_composite(self.reaction_text, (43, 295))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_modifier_reaction_frame, (30, 30))
                    card.image.alpha_composite(self.modifier_icon, (51, 138))
                    card.image.alpha_composite(self.reaction_icon, (51, 228))
                    card.image.alpha_composite(self.modifier_text, (43, 205))
                    card.image.alpha_composite(self.reaction_text, (43, 295))
                else:
                    card.image.alpha_composite(self.minion_modifier_reaction_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.modifier_icon, (51, 138))
                    card.image.alpha_composite(self.reaction_icon, (51, 228))
                    card.image.alpha_composite(self.modifier_text, (43, 205))
                    card.image.alpha_composite(self.reaction_text, (43, 295))
            elif minion_type == "CombatReaction":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_combat_reaction_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.combat_icon, (51, 138))
                    card.image.alpha_composite(self.reaction_icon, (51, 228))
                    card.image.alpha_composite(self.combat_text, (48, 205))
                    card.image.alpha_composite(self.reaction_text, (43, 295))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_combat_reaction_frame, (30, 30))
                    card.image.alpha_composite(self.combat_icon, (51, 138))
                    card.image.alpha_composite(self.reaction_icon, (51, 228))
                    card.image.alpha_composite(self.combat_text, (48, 205))
                    card.image.alpha_composite(self.reaction_text, (43, 295))
                else:
                    card.image.alpha_composite(self.minion_combat_reaction_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.combat_icon, (51, 138))
                    card.image.alpha_composite(self.reaction_icon, (51, 228))
                    card.image.alpha_composite(self.combat_text, (48, 205))
                    card.image.alpha_composite(self.reaction_text, (43, 295))
            elif minion_type == "Event":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_event_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.event_icon, (48, 138))
                    card.image.alpha_composite(self.event_text, (48, 237))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_event_frame, (30, 30))
                    card.image.alpha_composite(self.event_icon, (48, 138))
                    card.image.alpha_composite(self.event_text, (48, 237))
                else:
                    card.image.alpha_composite(self.minion_event_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.event_icon, (48, 138))
                    card.image.alpha_composite(self.event_text, (48, 237))
            elif minion_type == "Master":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.minion_master_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame.resize((550, 300)), (150, 680))
                    card.image.alpha_composite(self.master_text, (48, 205))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.minion_alt_master_frame, (30, 30))
                    card.image.alpha_composite(self.master_text, (48, 205))
                else:
                    card.image.alpha_composite(self.minion_master_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame.resize((650, 300), 1), (50, 680))
                    card.image.alpha_composite(self.master_text, (48, 205))


    def drawClan(self, card, width, height):
        clan = card.clan

        if clan and card.card_type == "Vampire":
            if clan == "None":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_clan_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_clan_frame, (30, 30))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_clan_frame, (30, 30))
                    if card.effect:
                        card.image.alpha_composite(self.textbox_frame_old.resize((637,128),1), (56, 845))
                else:
                    card.image.alpha_composite(self.vampire_clan_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
            elif clan == "Banu Haquim":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_banu_haquim_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.banu_haquim_icon, (38, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_banu_haquim_frame, (30, 30))
                    card.image.alpha_composite(self.banu_haquim_icon, (38, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_banu_haquim_frame, (30, 30))
                    card.image.alpha_composite(self.banu_haquim_icon, (52, 144))
                else:
                    card.image.alpha_composite(self.vampire_banu_haquim_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.banu_haquim_icon, (38, 144))
            elif clan == "Brujah":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_brujah_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.brujah_icon, (38, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_brujah_frame, (30, 30))
                    card.image.alpha_composite(self.brujah_icon, (38, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_brujah_frame, (30, 30))
                    card.image.alpha_composite(self.brujah_icon, (54, 144))
                    if card.effect:
                        card.image.alpha_composite(self.textbox_frame_old.resize((637,128),1), (56, 845))
                else:
                    card.image.alpha_composite(self.vampire_brujah_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.brujah_icon, (38, 144))
            elif clan == "Gangrel":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_gangrel_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.gangrel_icon, (35, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_gangrel_frame, (30, 30))
                    card.image.alpha_composite(self.gangrel_icon, (35, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_gangrel_frame, (30, 30))
                    card.image.alpha_composite(self.gangrel_icon, (54, 144))
                else:
                    card.image.alpha_composite(self.vampire_gangrel_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.gangrel_icon, (35, 144))
            elif clan == "Hecata":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_hecata_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.hecata_icon, (35, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_hecata_frame, (30, 30))
                    card.image.alpha_composite(self.hecata_icon, (35, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_hecata_frame, (30, 30))
                    card.image.alpha_composite(self.hecata_icon, (54, 144))
                    if card.effect:
                        card.image.alpha_composite(self.textbox_frame_old.resize((637,128),1), (56, 845))
                else:
                    card.image.alpha_composite(self.vampire_hecata_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.hecata_icon, (35, 145))
            elif clan == "Lasombra":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_lasombra_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.lasombra_icon, (44, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_lasombra_frame, (30, 30))
                    card.image.alpha_composite(self.lasombra_icon, (44, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_lasombra_frame, (30, 30))
                    card.image.alpha_composite(self.lasombra_icon, (59, 144))
                else:
                    card.image.alpha_composite(self.vampire_lasombra_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.lasombra_icon, (44, 145))
            elif clan == "Malkavian":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_malkavian_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.malkavian_icon, (54, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_malkavian_frame, (30, 30))
                    card.image.alpha_composite(self.malkavian_icon, (54, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_malkavian_frame, (30, 30))
                    card.image.alpha_composite(self.malkavian_icon, (62, 144))
                    if card.effect:  
                        card.image.alpha_composite(self.textbox_frame_old.resize((637,128),1), (56, 845))
                else:
                    card.image.alpha_composite(self.vampire_malkavian_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.malkavian_icon, (54, 144))
            elif clan == "Ministry":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_ministry_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.ministry_icon, (30, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_ministry_frame, (30, 30))
                    card.image.alpha_composite(self.ministry_icon, (30, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_ministry_frame, (30, 30))
                    card.image.alpha_composite(self.ministry_icon, (49, 144))
                else:
                    card.image.alpha_composite(self.vampire_ministry_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.ministry_icon, (30, 144))
            elif clan == "Nosferatu":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_nosferatu_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.nosferatu_icon, (50, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_nosferatu_frame, (30, 30))
                    card.image.alpha_composite(self.nosferatu_icon, (50, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_nosferatu_frame, (30, 30))
                    card.image.alpha_composite(self.nosferatu_icon, (54, 144))
                    if card.effect:
                        card.image.alpha_composite(self.textbox_frame_old.resize((637,128),1), (56, 845))
                else:
                    card.image.alpha_composite(self.vampire_nosferatu_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.nosferatu_icon, (50, 144))
            elif clan == "Ravnos":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_ravnos_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.ravnos_icon, (38, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_ravnos_frame, (30, 30))
                    card.image.alpha_composite(self.ravnos_icon, (38, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_ravnos_frame, (30, 30))
                    card.image.alpha_composite(self.ravnos_icon, (49, 144))
                else:
                    card.image.alpha_composite(self.vampire_ravnos_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.ravnos_icon, (38, 144))
            elif clan == "Salubri":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_salubri_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.salubri_icon, (38, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_salubri_frame, (30, 30))
                    card.image.alpha_composite(self.salubri_icon, (38, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_salubri_frame, (30, 30))
                    card.image.alpha_composite(self.salubri_icon, (49, 144))
                else:
                    card.image.alpha_composite(self.vampire_salubri_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.salubri_icon, (38, 144))
            elif clan == "Toreador":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_toreador_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.toreador_icon, (38, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_toreador_frame, (30, 30))
                    card.image.alpha_composite(self.toreador_icon, (38, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_toreador_frame, (30, 30))
                    card.image.alpha_composite(self.toreador_icon, (54, 144))
                    if card.effect:
                        card.image.alpha_composite(self.textbox_frame_old.resize((637,128),1), (56, 845))
                else:
                    card.image.alpha_composite(self.vampire_toreador_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.toreador_icon, (38, 144))
            elif clan == "Tremere":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_tremere_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.tremere_icon, (38, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_tremere_frame, (30, 30))
                    card.image.alpha_composite(self.tremere_icon, (38, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_tremere_frame, (30, 30))
                    card.image.alpha_composite(self.tremere_icon, (54, 144))
                else:
                    card.image.alpha_composite(self.vampire_tremere_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.tremere_icon, (38, 144))
            elif clan == "Tzimisce":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_tzimisce_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.tzimisce_icon, (43, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_tzimisce_frame, (30, 30))
                    card.image.alpha_composite(self.tzimisce_icon, (43, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_tzimisce_frame, (30, 30))
                    card.image.alpha_composite(self.tzimisce_icon, (59, 144))
                else:
                    card.image.alpha_composite(self.vampire_tzimisce_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.tzimisce_icon, (43, 144))
            elif clan == "Ventrue":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.vampire_ventrue_frame, (30, 30))
                    card.image.alpha_composite(self.alt_textbox_frame, (150, 830))
                    card.image.alpha_composite(self.ventrue_icon, (38, 144))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.vampire_alt_ventrue_frame, (30, 30))
                    card.image.alpha_composite(self.ventrue_icon, (38, 144))
                elif card.variant == "Old":
                    card.image.alpha_composite(self.vampire_old_ventrue_frame, (30, 30))
                    card.image.alpha_composite(self.ventrue_icon, (54, 144))
                else:
                    card.image.alpha_composite(self.vampire_ventrue_frame, (30, 30))
                    card.image.alpha_composite(self.textbox_frame, (50, 830))
                    card.image.alpha_composite(self.ventrue_icon, (38, 144))
        elif clan and card.card_type == "Minion":
            if clan == "Banu Haquim":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.banu_haquim_icon, (38, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.banu_haquim_icon, (38, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.banu_haquim_icon, (38, 346))
                    else:
                        card.image.alpha_composite(self.banu_haquim_icon, (38, 366))
            elif clan == "Brujah":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.brujah_icon, (38, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.brujah_icon, (38, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.brujah_icon, (38, 346))
                    else:
                        card.image.alpha_composite(self.brujah_icon, (38, 366))
            elif clan == "Gangrel":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.gangrel_icon, (35, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.gangrel_icon, (35, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.gangrel_icon, (35, 366))
                    else:
                        card.image.alpha_composite(self.gangrel_icon, (35, 366))
            elif clan == "Hecata":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.hecata_icon, (35, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.hecata_icon, (35, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.hecata_icon, (35, 346))
                    else:
                        card.image.alpha_composite(self.hecata_icon, (35, 366))
            elif clan == "Lasombra":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.lasombra_icon, (39, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.lasombra_icon, (39, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.lasombra_icon, (39, 346))
                    else:
                        card.image.alpha_composite(self.lasombra_icon, (39, 366))
            elif clan == "Malkavian":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.malkavian_icon, (54, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.malkavian_icon, (54, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.malkavian_icon, (54, 346))
                    else:
                        card.image.alpha_composite(self.malkavian_icon, (54, 366))
            elif clan == "Ministry":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.ministry_icon, (30, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.ministry_icon, (30, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.ministry_icon, (30, 346))
                    else:
                        card.image.alpha_composite(self.ministry_icon, (30, 366))
            elif clan == "Nosferatu":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.nosferatu_icon, (50, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.nosferatu_icon, (50, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.nosferatu_icon, (50, 346))
                    else:
                        card.image.alpha_composite(self.nosferatu_icon, (50, 366))
            elif clan == "Ravnos":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.ravnos_icon, (38, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.ravnos_icon, (38, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.ravnos_icon, (38, 346))
                    else:
                        card.image.alpha_composite(self.ravnos_icon, (38, 366))
            elif clan == "Salubri":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.salubri_icon, (38, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.salubri_icon, (38, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.salubri_icon, (38, 346))
                    else:
                        card.image.alpha_composite(self.salubri_icon, (38, 366))
            elif clan == "Toreador":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.toreador_icon, (38, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.toreador_icon, (38, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.toreador_icon, (38, 346))
                    else:
                        card.image.alpha_composite(self.toreador_icon, (38, 366))
            elif clan == "Tremere":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.tremere_icon, (38, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.tremere_icon, (38, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.tremere_icon, (38, 346))
                    else:
                        card.image.alpha_composite(self.tremere_icon, (38, 366))
            elif clan == "Tzimisce":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.tzimisce_icon, (38, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.tzimisce_icon, (38, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.tzimisce_icon, (38, 346))
                    else:
                        card.image.alpha_composite(self.tzimisce_icon, (38, 366))
            elif clan == "Ventrue":
                if card.variant == "Alternate":
                    card.image.alpha_composite(self.ventrue_icon, (38, 366))
                elif card.variant == "Classic":
                    card.image.alpha_composite(self.ventrue_icon, (38, 366))
                else:
                    if len(card.discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
                        card.image.alpha_composite(self.ventrue_icon, (38, 346))
                    else:
                        card.image.alpha_composite(self.ventrue_icon, (38, 366))

    def parseEffect(self, card):
        if card.effect == "":
            return []

        split_effect = list(filter(None, re.split(r"( |\\|\.|,|<.*?>|\[.*?\]|\n)", card.effect)))
        parsed_effect = []
        font_mode = "Standard"
        centered = False

        # Parsed word is [text, font, centered, size(used in formatting and writting)] #

        for word in split_effect:
            if word == "[B]":
                font_mode = "Bold"
            elif word == "[I]":
                font_mode = "Italic"
            elif word == "[b]" and font_mode == "Bold":
                font_mode = "Standard"
            elif word == "[i]" and font_mode == "Italic":
                font_mode = "Standard"
            elif word == "[C]":
                centered = True
            elif word in ["\n", "\\"]:
                if parsed_effect and parsed_effect[-1] == ["", "Newline", False, 0]:
                    parsed_effect[-1] = ["", "Newblock", False, 0]
                else:
                    centered = False
                    parsed_effect.append(["", "Newline", False, 0])
            elif word[0] == "[" and word[-1] == "]":
                parsed_effect.append([word[1:-1], "Icon", centered, 0])
            else:
                parsed_effect.append([word, font_mode, centered, 0])

        return parsed_effect

    def formatEffect(self, card, parsed_effect, font_size=44, min_width=54, max_width=698, max_height=158 ,offset=0):
        if card.card_type == "Minion":
            if card.variant == "Classic" or exists("Template/Custom/Classic Minion/" + card.variant):
                max_height = 350
                max_width = 688
                min_width=168
            elif card.variant == "FullBleed" or exists("Template/Custom/Minion/" + card.variant):
                max_height = 300
            elif card.variant == "Alternate" or exists("Template/Custom/Minion/" + card.variant):
                max_height = 300
                max_width = 688
                min_width = 154
        if card.card_type == "Vampire":
            if card.variant == "Classic" or exists("Template/Custom/Classic Vampire/" + card.variant):
                min_width=154
            elif card.variant == "Alternate"or exists("Template/Custom/Alternate Vampire/" + card.variant):
                min_width= 152
            elif card.variant == "Old"or exists("Template/Custom/Old Vampire/" + card.variant):
                min_width= 66 
                max_height = 128
                
        if parsed_effect == []:
                    return [], 0
        font_size_computed = False
        line_width = max_width - min_width
        final_height = max_height - offset

        while not font_size_computed:
            formatted_effect = []
            current_line = []
            width = 0

            new_lines = 0
            new_blocks = 0

            effect_standard_font = ImageFont.truetype(self.effect_font_path, font_size)
            effect_bold_font = ImageFont.truetype(self.effect_bold_font_path, font_size)
            effect_italic_font = ImageFont.truetype(self.effect_italic_font_path, font_size)

            for word in parsed_effect:
                if word[1] == "Newblock" or word[1] == "Newline":
                    if word[1] == "Newblock":
                        new_blocks += 1
                    else:
                        new_lines += 1
                    word[3] = width
                    current_line.append(word)
                    formatted_effect.append(current_line)
                    current_line = []
                    width = 0
                    continue

                if word[1] == "Bold":
                    word_font = effect_bold_font
                elif word[1] == "Italic":
                    word_font = effect_italic_font
                else:
                    word_font = effect_standard_font

                text = word[0]
                # Handle icons #
                if word[1] == "Icon":
                    if word[0] == "D":
                        text = "  "
                    if word[0] == "Merged":
                        text = "  "
                    # handle discipline icons
                    elif word[0] in ["ANI", "AUS", "BLO", "CEL", "CHI", "DAI", "DEM", "DOM", "FOR", "MEL", "MYT", "NEC", "OBE", "OBF", "OBL", "PRO", "PRE", "POT", "QUI", "SER", "THA"  "VIC","ani", "aus", "blo", "cel", "chi", "dai", "dem", "dom", "for", "mel", "myt", "nec", "obe", "obf", "obl", "pro", "pre", "pot", "qui", "ser", "tha", "vic"]:
                    # handle clan icons
                        text = "   "
                    elif word[0] in ["Ban", "Bru", "Gan", "Hec", "Min", "Sal", "Tor", "Tre", "Tzi","Ven"]:
                        text = "   "
                    elif word[0] in ["aCt", "rEa", "mOd", "cOm", "rEf", "aLl", "rEt"]:
                        text = "   "
                    elif word[0] in ["Mal", "Nos"]:
                        text = "  "
                    # blood, !blood or dunno
                    elif word[0] in ["H"]:
                        text = ""
                    else:
                        text = "    "

                word_box = card.draw.textbbox((0,0),text, font=word_font)
                word_width = word_box [2] - word_box[0]

                if width + word_width > line_width:
                    new_lines += 1
                    if text == " ":
                        formatted_effect.append(current_line)
                        current_line = []
                        width = 0
                        word[3] = 0
                    elif text == ".":
                        last_word = current_line.pop(-1)
                        formatted_effect.append(current_line)
                        current_line = [last_word, word]
                        last_word[3] = 0
                        width_box = card.draw.textbbox((0,0),last_word[0], font=word_font)
                        width = width_box[2]- width_box[0]
                        word[3] = width
                        width += word_width
                    else:
                        formatted_effect.append(current_line)
                        current_line = [word]
                        width = word_width
                        word[3] = 0

                else:
                    word[3] = width
                    width += word_width
                    current_line.append(word)

            formatted_effect.append(current_line)

            lines = len(formatted_effect)
            height = ceil(font_size * (lines + (new_lines * 4.0/19 + new_blocks * 12.0/19))) + lines # Add lines to fix pixel loss (dunno why but it works)

            if height <= final_height:
                font_size_computed = True

            else:
                font_size -= 1

        card.font_size = font_size

        return formatted_effect, height

    def drawEffect(self, card, formatted_effect, min_width=56, max_width=696, height=835):
        if formatted_effect == []:
            return
        if card.card_type == "Vampire":
            if card.variant == "Classic" or exists("Template/Custom/Classic Vampire/" + card.variant):
                min_width= 157
                height= 835
            elif card.variant == "Old" or exists("Template/Custom/Old Vampire/" + card.variant):
                min_width= 66 
                height= 855    
            elif card.variant == "Alternate" or exists("Template/Custom/Alternate Vampire/" + card.variant):
                min_width = 154
        if card.card_type == "Minion":
            if card.variant == "Classic" or exists("Template/Custom/Classic Minion/" + card.variant):
                min_width=168
                max_width=688
                height= 664
            elif card.variant == "FullBleed" or exists("Template/Custom/Minion/" + card.variant):
                max_width=688
                height= 686
            elif card.variant == "Alternate" or exists("Template/Custom/Alternate Minion/" + card.variant):
                min_width = 154
                max_width=688
                height= 686
        image = card.image
        draw = card.draw
        font_size = card.font_size

        line_width = max_width - min_width

        newline_height = font_size + ceil(font_size * 2/19)
        newblock_diff = font_size + ceil(font_size * 9/19) - newline_height

        effect_standard_font = ImageFont.truetype(self.effect_font_path, font_size)
        effect_bold_font = ImageFont.truetype(self.effect_bold_font_path, font_size)
        effect_italic_font = ImageFont.truetype(self.effect_italic_font_path, font_size)

        ratio = font_size / 32
        height_offset = 0

        for line in formatted_effect:
            center_offset = 0
            center_computed = False

            for word in line:
                if word[1] == "Newblock":
                    height_offset += newblock_diff
                    continue

                if word[1] == "Bold":
                    word_font = effect_bold_font
                elif word[1] == "Italic":
                    word_font = effect_italic_font
                else:
                    word_font = effect_standard_font

                if word[2]:
                    if not center_computed:
                        font_type = line[-1][1]
                        if font_type == "Bold":
                            last_word_font = effect_bold_font
                        elif font_type == "Italic":
                            last_word_font = effect_italic_font
                        else:
                            last_word_font = effect_standard_font
                        
                        center_offset = floor((line_width - line[-1][3] - (draw.textbbox((0,0),line[-1][0], font=last_word_font)[2]) - (draw.textbbox((0,0),line[-1][0], font=last_word_font)[0])) / 2)
                        center_computed = True
                        
                else:
                    center_offset = 0
                    center_computed = False

                text = word[0]
                width_offset = word[3] + center_offset

                if word[1] == "Icon":
                    # ToDo adjust icons #
                    if text == "D":
                        text_icon = Image.open("Template/Discipline Icons/Directed.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "Merged":
                        text_icon = Image.open("Template/Discipline Icons/Merged.png").resize((int(24 * ratio), int(30 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset + 2 ))
                    elif text == "H":
                        height_offset +=10
                    # Discipline icons
                    elif text == "VIS":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorVisceratika.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "vis":
                        text_icon = Image.open("Template/Discipline Icons/InferiorVisceratika.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "VIC":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorVicissitude.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "vic":
                        text_icon = Image.open("Template/Discipline Icons/InferiorVicissitude.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "VAL":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorValeren.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "val":
                        text_icon = Image.open("Template/Discipline Icons/InferiorValeren.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "THN":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorThanatosis.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "THA":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorThaumaturgy.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "tha":
                        text_icon = Image.open("Template/Discipline Icons/InferiorThaumaturgy.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "thn":
                        text_icon = Image.open("Template/Discipline Icons/InferiorThanatosis.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "TEM":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorTemporis.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "tem":
                        text_icon = Image.open("Template/Discipline Icons/InferiorTemporis.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "SPI":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorSpiritus.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "spi":
                        text_icon = Image.open("Template/Discipline Icons/InferiorSpiritus.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "SER":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorSerpentis.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "ser":
                        text_icon = Image.open("Template/Discipline Icons/InferiorSerpentis.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "SAN":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorSanguinus.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "san":
                        text_icon = Image.open("Template/Discipline Icons/InferiorSanguinus.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "QUI":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorQuietus.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "qui":
                        text_icon = Image.open("Template/Discipline Icons/InferiorQuietus.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "PRO":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorProtean.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "pro":
                        text_icon = Image.open("Template/Discipline Icons/InferiorProtean.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "PRE":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorPresence.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "pre":
                        text_icon = Image.open("Template/Discipline Icons/InferiorPresence.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "POT":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorPotence.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "pot":
                        text_icon = Image.open("Template/Discipline Icons/InferiorPotence.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "OBT":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorObtenebration.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "obt":
                        text_icon = Image.open("Template/Discipline Icons/InferiorObtenebration.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "OBL":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorOblivion.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "obl":
                        text_icon = Image.open("Template/Discipline Icons/InferiorOblivion.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "OBF":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorObfuscate.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "obf":
                        text_icon = Image.open("Template/Discipline Icons/InferiorObfuscate.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "OBE":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorObeah.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "obe":
                        text_icon = Image.open("Template/Discipline Icons/InferiorObeah.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "NEC":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorNecromancy.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "nec":
                        text_icon = Image.open("Template/Discipline Icons/InferiorNecromancy.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "MYT":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorMytherceria.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "myt":
                        text_icon = Image.open("Template/Discipline Icons/InferiorMytherceria.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "MEL":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorMelopomine.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "mel":
                        text_icon = Image.open("Template/Discipline Icons/InferiorMelopomine.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "FOR":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorFortitude.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "for":
                        text_icon = Image.open("Template/Discipline Icons/InferiorFortitude.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "DOM":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorDominate.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "dom":
                        text_icon = Image.open("Template/Discipline Icons/InferiorDominate.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "DEM":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorDementation.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "dem":
                        text_icon = Image.open("Template/Discipline Icons/InferiorDementation.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "DAI":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorDaimonion.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "dai":
                        text_icon = Image.open("Template/Discipline Icons/InferiorDaimonion.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "CHI":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorChimerstry.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "chi":
                        text_icon = Image.open("Template/Discipline Icons/InferiorChimerstry.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "CEL":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorCelerity.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "cel":
                        text_icon = Image.open("Template/Discipline Icons/InferiorCelerity.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text in ["BLO", "THA"]:
                        text_icon = Image.open("Template/Discipline Icons/SuperiorBloodSorcery.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text in ["blo", "tha"]:
                        text_icon = Image.open("Template/Discipline Icons/InferiorBloodSorcery.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "AUS":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorAuspex.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "aus":
                        text_icon = Image.open("Template/Discipline Icons/InferiorAuspex.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "ANI":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorAnimalism.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "ani":
                        text_icon = Image.open("Template/Discipline Icons/InferiorAnimalism.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "ABO":
                        text_icon = Image.open("Template/Discipline Icons/SuperiorAbombwe.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(6 * ratio))))
                    elif text == "abo":
                        text_icon = Image.open("Template/Discipline Icons/InferiorAbombwe.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "fli":
                        text_icon = Image.open("Template/Discipline Icons/Flight.png").resize((int(32 * ratio * 1.3), int(24 * ratio * 1.3)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    # Clan Icons #
                    elif text == "Ban":
                        text_icon = Image.open("Template/Vampire/BanuHaquimIcon.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -6 ))
                    elif text == "Bru":
                        text_icon = Image.open("Template/Vampire/BrujahIcon.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "Gan":
                        text_icon = Image.open("Template/Vampire/GangrelIcon.png").resize((int(32 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "Hec":
                        text_icon = Image.open("Template/Vampire/HecataIcon.png").resize((int(30 * ratio), int(40 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "Las":
                        text_icon = Image.open("Template/Vampire/LasombraIcon.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "Mal":
                        text_icon = Image.open("Template/Vampire/MalkavianIcon.png").resize((int(20 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "Min":
                        text_icon = Image.open("Template/Vampire/MinistryIcon.png").resize((int(40 * ratio), int(25 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset +(int(2 * ratio))))
                    elif text == "Nos":
                        text_icon = Image.open("Template/Vampire/NosferatuIcon.png").resize((int(20 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "Rav":
                        text_icon = Image.open("Template/Vampire/RavnosIcon.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "Sal":
                        text_icon = Image.open("Template/Vampire/SalubriIcon.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "Tor":
                        text_icon = Image.open("Template/Vampire/ToreadorIcon.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "Tre":
                        text_icon = Image.open("Template/Vampire/TremereIcon.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "Tzi":
                        text_icon = Image.open("Template/Vampire/TzimisceIcon.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "Ven":
                        text_icon = Image.open("Template/Vampire/VentrueIcon.png").resize((int(35 * ratio), int(35 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "aCt":
                        text_icon = Image.open("Template/Minion/Action.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "rEa":
                        text_icon = Image.open("Template/Minion/Reaction.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "mOd":
                        text_icon = Image.open("Template/Minion/Modifier.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "cOm":
                        text_icon = Image.open("Template/Minion/Combat.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "rEf":
                        text_icon = Image.open("Template/Minion/Reflex.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "aLl":
                        text_icon = Image.open("Template/Minion/Ally.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))
                    elif text == "rEt":
                        text_icon = Image.open("Template/Minion/Retainer.png").resize((int(32 * ratio), int(32 * ratio)), 1)
                        image.alpha_composite(text_icon, (min_width + width_offset - 4, height + height_offset -(int(5 * ratio))))


                    # blood, !blood or dunno

                    elif len(text):
                        if text[0] != "!":
                            text_numeric_cost = Image.open("Template/Text Box Icons/Numeric Cost.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                            cost_font = ImageFont.truetype(self.cost_font_path, font_size - 4)

                            # Handle plural digits #
                            text_len = len(text)
                            size_offset = 0
                            if text_len > 1:
                                size_offset -= draw.textbbox((0,0),text, font=cost_font)[0] / text_len * (text_len - 1) / 2

                            image.alpha_composite(text_numeric_cost, (min_width + width_offset - int(5 * ratio), height + height_offset + int(4 * ratio)))
                            draw.text((min_width + width_offset + size_offset + int(8 * ratio), height + height_offset + int(10 * ratio)), text, font=cost_font, fill=(255, 255, 255, 255))

                        else:
                            text = text[1:-1]
                            text_numeric_cost = Image.open("Template/Text Box Icons/Numeric Cost Invert.png").resize((int(36 * ratio), int(36 * ratio)), 1)
                            cost_font = ImageFont.truetype(self.cost_font_path, font_size - 4)

                            # Handle plural digits #
                            text_len = len(text)
                            size_offset = 0
                            if text_len > 1:
                                size_offset -= draw.textbbox((0,0),text, font=cost_font)[0] / text_len * (text_len - 1) / 2

                            image.alpha_composite(text_numeric_cost, (min_width + width_offset - int(5 * ratio), height + height_offset + int(4 * ratio)))
                            draw.text((min_width + width_offset + size_offset + int(8 * ratio), height + height_offset + int(10 * ratio)), text, font=cost_font, fill=(0, 0, 0, 255))
                    # No text #
                    else:
                        pass

                else:
                    draw.text((min_width + width_offset, height + height_offset), text, font=word_font, fill=(0, 0, 0, 255))

            height_offset += newline_height

    def drawBackground(self, card):
        # Draw text background if any #
        if card.background_path:
            try:
                bg_art = Image.open(card.background_path)
                card.image.alpha_composite(bg_art)
            except Exception:
                print("Could not find background file")

    def formatFlavour(self, card, font_size=20, min_width=146, max_width=604):
        if card.flavour == "":
            return False

        font_size_computed = False
        flavor_width = max_width - min_width
        split_flavour = re.split("(\s+|;|\n+)", card.flavour)
        
        # Sanitize flavour #
        while split_flavour != [] and split_flavour[-1] in ["", " ", ";", "\n"]:
            split_flavour.pop(-1)

        if split_flavour == []:
            return False

        while not font_size_computed:
            flavour_font = ImageFont.truetype(self.flavour_font_path, font_size)

            height = font_size
            lines_list = []
            current_line = ""

            newline_height = font_size + ceil(font_size * 4/19)

            for word in split_flavour:
                if (len(current_line) == 0 and word == " ") or word == "":
                    pass

                elif word == ";" or word == "\n":
                    
                    if current_line and current_line[-1] == " ":
                        current_line = current_line[:-1]

                    lines_list.append(current_line)
                    current_line = ""
                    height += newline_height

                else:
                    current_line += word

            if not current_line:
                current_line = lines_list[-1]

            if current_line[-1] == " ":
                current_line = current_line[:-1]

            lines_list.append(current_line)

            font_size_computed = True
            for line in lines_list:
                if card.draw.textbbox((0,0),line, flavour_font)[0] > flavor_width:
                    font_size_computed = False
                    font_size -= 1

        card.font_size = font_size
        card.lines_list = lines_list

        return True

    def drawFlavour(self, card, height=900):
        draw = card.draw
        font_size = card.font_size
        # Draw #
        newline_height = font_size + ceil(font_size * 4/19) - 3 # Strange fix

        flavour_font = ImageFont.truetype(self.flavour_font_path, font_size)

        nb_lines = len(card.lines_list)

        height_offset = 22 * (3 - nb_lines)

        for line in card.lines_list:
            line_size = draw.textbbox((0,0),line, flavour_font)[0]
            target_width = floor(375 - (line_size / 2.0))
            draw.text((target_width, height + height_offset), line, font=flavour_font, fill=(0, 0, 0, 255))
            height_offset += newline_height

        return 22 * nb_lines

    def drawDisciplines(self, card, width=47, height=748):
        image = card.image
        card.discipline_icons == []
        discipline_icons = card.discipline_icons
        if card.variant in ["Alternate", "Classic"] or exists("Template/Custom/Classic Vampire/" + card.variant) or exists("Template/Custom/Alternate Vampire/" + card.variant):
            height += 110
        elif card.variant in ["Alternate", "Classic"] or exists("Template/Custom/Classic Minion/" + card.variant) or exists("Template/Custom/Alternate Minion/" + card.variant):
            height -= 410
        elif card.variant == "Old" or exists("Template/Custom/Old Vampire/" + card.variant):
            height += 2
            width += 572
        if card.card_type == "Minion":
            height -= 387
            if card.variant not in ["FullBleed", "Alternate", "Classic", "Old"] and exists ("Template/Custom/Classic Minion/" + card.variant):
                height += 517
            if card.card_type == "Minion" and card.variant == "FullBleed" or exists("Template/Custom/Minion/" + card.variant):
                height += 36
                if card.clan != "None":
                    height += 74


        icon_offset = height
        is_superior = bool
        is_superior = False
        height_o = 761
        width_o = 640
        reversed_disciplines = []
        sorted_disciplines = []

        if len(discipline_icons) == 2 and card.card_type == "Minion" and card.variant == "FullBleed":
            icon_offset -= 24
            if not card.clan == "None":
                icon_offset +=20
        elif len(discipline_icons) == 3 and card.card_type == "Minion" and card.variant == "FullBleed":
            icon_offset -= 48
            if not card.clan == "None":
                icon_offset +=20
        if card.card_type == "Vampire" and not card.variant == "Old":
            for discipline_name in reversed(sorted(discipline_icons)):
                if icon_offset < 0:
                    break
                if discipline_name == "Superior Visceratika":
                    image.alpha_composite(self.superior_visceratika_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Vicissitude":
                    image.alpha_composite(self.superior_vicissitude_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Valeren":
                    image.alpha_composite(self.superior_valeren_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Thaumaturgy":
                    image.alpha_composite(self.superior_thaumaturgy_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Thanatosis":
                    image.alpha_composite(self.superior_thanatosis_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Temporis":
                    image.alpha_composite(self.superior_temporis_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Spiritus":
                    image.alpha_composite(self.superior_spiritus_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Serpentis":
                    image.alpha_composite(self.superior_serpentis_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Sanguinus":
                    image.alpha_composite(self.superior_sanguinus_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Quietus":
                    image.alpha_composite(self.superior_quietus_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Protean":
                    image.alpha_composite(self.superior_protean_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Presence":
                    image.alpha_composite(self.superior_presence_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Potence":
                    image.alpha_composite(self.superior_potence_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Obtenebration":
                    image.alpha_composite(self.superior_obtenebration_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Oblivion":
                    image.alpha_composite(self.superior_oblivion_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Obfuscate":
                    image.alpha_composite(self.superior_obfuscate_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Obeah":
                    image.alpha_composite(self.superior_obeah_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Necromancy":
                    image.alpha_composite(self.superior_necromancy_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Mytherceria":
                    image.alpha_composite(self.superior_mytherceria_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Melopomine":
                    image.alpha_composite(self.superior_melopomine_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Fortitude":
                    image.alpha_composite(self.superior_fortitude_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Dominate":
                    image.alpha_composite(self.superior_dominate_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Dementation":
                    image.alpha_composite(self.superior_dementation_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Daimonion":
                    image.alpha_composite(self.superior_daimonion_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Chimerstry":
                    image.alpha_composite(self.superior_chimerstry_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Celerity":
                    image.alpha_composite(self.superior_celerity_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Blood Sorcery":
                    image.alpha_composite(self.superior_blood_sorcery_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Auspex":
                    image.alpha_composite(self.superior_auspex_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Animalism":
                    image.alpha_composite(self.superior_animalism_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Superior Abombwe":
                    image.alpha_composite(self.superior_abombwe_icon, (width, icon_offset))
                    icon_offset -= 74
                    is_superior = True
                elif discipline_name == "Inferior Visceratika":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Visceratika") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_visceratika_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Vicissitude":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Vicissitude") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_vicissitude_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Valeren":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Valeren") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_valeren_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Thaumaturgy":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Thaumaturgy") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_thaumaturgy_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Thanatosis":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Thanatosis") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_thanatosis_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Temporis":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Temporis") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_temporis_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Spiritus":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Spiritus") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_spiritus_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Serpentis":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Serpentis") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_serpentis_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Sanguinus":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Sanguinus") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_sanguinus_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Quietus":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Quietus") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_quietus_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Protean":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Protean") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_protean_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Presence":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Presence") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_presence_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Potence":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Potence") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_potence_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Obtenebration":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Obtenebration") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_obtenebration_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Oblivion":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Oblivion") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_oblivion_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Obfuscate":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Obfuscate") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_obfuscate_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Obeah":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Obeah") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_obeah_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Necromancy":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Necromancy") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_necromancy_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Mytherceria":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Mytherceria") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_mytherceria_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Melopomine":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Melopomine") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_melopomine_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Fortitude":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Fortitude") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_fortitude_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Dominate":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Dominate") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_dominate_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Dementation":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Dementation") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_dementation_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Daimonion":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Daimonion") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_daimonion_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Chimerstry":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Chimerstry") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_chimerstry_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Celerity":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Celerity") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_celerity_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Blood Sorcery":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Blood Sorcery") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_blood_sorcery_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Auspex":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Auspex") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_auspex_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Animalism":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Animalism") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_animalism_icon, (width+6, icon_offset))
                    icon_offset -= 67
                elif discipline_name == "Inferior Abombwe":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Abombwe") == 0:
                        icon_offset +=6
                    if is_superior == True:
                        icon_offset +=10
                        is_superior = False
                    image.alpha_composite(self.inferior_abombwe_icon, (width+6, icon_offset))
                    icon_offset -= 67
        elif card.card_type == "Minion":
            for discipline_name in sorted(discipline_icons):
                if discipline_name == "Flight":
                    image.alpha_composite(self.flight_icon.resize((64, 44), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Abombwe":
                    image.alpha_composite(self.inferior_abombwe_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Animalism":
                    image.alpha_composite(self.inferior_animalism_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Auspex":
                    image.alpha_composite(self.inferior_auspex_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Blood Sorcery":
                    image.alpha_composite(self.inferior_blood_sorcery_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Celerity":
                    image.alpha_composite(self.inferior_celerity_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Chimerstry":
                    image.alpha_composite(self.inferior_chimerstry_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Daimonion":
                    image.alpha_composite(self.inferior_daimonion_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Dementation":
                    image.alpha_composite(self.inferior_dementation_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Dominate":
                    image.alpha_composite(self.inferior_dominate_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Fortitude":
                    image.alpha_composite(self.inferior_fortitude_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Melopomine":
                    image.alpha_composite(self.inferior_melopomine_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Mytherceria":
                    image.alpha_composite(self.inferior_mytherceria_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Necromancy":
                    image.alpha_composite(self.inferior_necromancy_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Obfuscate":
                    image.alpha_composite(self.inferior_obfuscate_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Obeah":
                    image.alpha_composite(self.inferior_obeah_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Oblivion":
                    image.alpha_composite(self.inferior_oblivion_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Obtenebration":
                    image.alpha_composite(self.inferior_obtenebration_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Potence":
                    image.alpha_composite(self.inferior_potence_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Presence":
                    image.alpha_composite(self.inferior_presence_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Protean":
                    image.alpha_composite(self.inferior_protean_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Quietus":
                    image.alpha_composite(self.inferior_quietus_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Sanguinus":
                    image.alpha_composite(self.inferior_sanguinus_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Serpentis":
                    image.alpha_composite(self.inferior_serpentis_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Spiritus":
                    image.alpha_composite(self.inferior_spiritus_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Temporis":
                    image.alpha_composite(self.inferior_temporis_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Thanatosis":
                    image.alpha_composite(self.inferior_thanatosis_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Thaumaturgy":
                    image.alpha_composite(self.inferior_thaumaturgy_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Valeren":
                    image.alpha_composite(self.inferior_valeren_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
                elif discipline_name == "Inferior Vicissitude":
                    image.alpha_composite(self.inferior_vicissitude_icon.resize((64, 64), 1), (width+3, icon_offset))
                elif discipline_name == "Inferior Visceratika":
                    image.alpha_composite(self.inferior_visceratika_icon.resize((64, 64), 1), (width+3, icon_offset))
                    icon_offset += 74
        elif card.card_type == "Vampire" and card.variant == "Old":
            for discipline_name in discipline_icons:
                if discipline_name == "Superior Visceratika":
                    height_o = 761
                    width_o = 640
                    image.alpha_composite(self.superior_visceratika_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Vicissitude":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))    
                    if reversed_disciplines.index("Superior Vicissitude") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Vicissitude") == 1:
                        height_o = 761
                        width_o = 586
                    image.alpha_composite(self.superior_vicissitude_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Valeren":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Superior Valeren") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Valeren") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Valeren") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 528
                        else:
                            height_o = 710
                            width_o = 640
                    image.alpha_composite(self.superior_valeren_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Thaumaturgy":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Superior Thaumaturgy") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Thaumaturgy") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Thaumaturgy") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Thaumaturgy") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Thaumaturgy") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Thaumaturgy") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_thaumaturgy_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Thanatosis":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Superior Thanatosis") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Thanatosis") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Thanatosis") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Thanatosis") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Thanatosis") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Thanatosis") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_thanatosis_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Temporis":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Superior Temporis") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Temporis") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Temporis") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Temporis") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Temporis") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Temporis") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_temporis_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Spiritus":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Superior Spiritus") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Spiritus") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Spiritus") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Spiritus") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Spiritus") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Spiritus") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_spiritus_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Serpentis":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Superior Serpentis") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Serpentis") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Serpentis") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Serpentis") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Serpentis") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Serpentis") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_serpentis_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Sanguinus":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Superior Sanguinus") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Sanguinus") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Sanguinus") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Sanguinus") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Sanguinus") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Sanguinus") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_sanguinus_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Quietus":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Quietus") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Quietus") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Quietus") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Quietus") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Quietus") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Quietus") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_quietus_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Protean":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Protean") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Protean") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Protean") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Protean") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Protean") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Protean") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_protean_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Presence":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Presence") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Presence") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Presence") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Presence") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Presence") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Presence") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_presence_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Potence":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Potence") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Potence") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Potence") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Potence") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Potence") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Potence") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_potence_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Obtenebration":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Obtenebration") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Obtenebration") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Obtenebration") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Obtenebration") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Obtenebration") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Obtenebration") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_obtenebration_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Oblivion":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Oblivion") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Oblivion") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Oblivion") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Oblivion") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Oblivion") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Oblivion") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_oblivion_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Obfuscate":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Obfuscate") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Obfuscate") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Obfuscate") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Obfuscate") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Obfuscate") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Obfuscate") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_obfuscate_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Obeah":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Superior Obeah") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Obeah") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Obeah") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Obeah") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Obeah") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Obeah") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_obeah_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Necromancy":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Necromancy") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Necromancy") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Necromancy") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Necromancy") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Necromancy") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Necromancy") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_necromancy_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Mytherceria":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Superior Mytherceria") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Mytherceria") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Mytherceria") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Mytherceria") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Mytherceria") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Mytherceria") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_mytherceria_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Melopomine":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Superior Melopomine") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Melopomine") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Melopomine") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Melopomine") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Melopomine") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Melopomine") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_melopomine_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Fortitude":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Fortitude") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Fortitude") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Fortitude") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Fortitude") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Fortitude") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Fortitude") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_fortitude_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Dominate":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Dominate") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Dominate") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Dominate") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Dominate") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Dominate") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Dominate") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_dominate_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Dementation":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Dementation") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Dementation") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Dementation") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Dementation") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Dementation") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Dementation") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_dementation_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Daimonion":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Superior Daimonion") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Daimonion") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Daimonion") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Daimonion") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Daimonion") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Daimonion") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_daimonion_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Chimerstry":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Chimerstry") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Chimerstry") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Chimerstry") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Chimerstry") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Chimerstry") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Chimerstry") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_chimerstry_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Celerity":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Celerity") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Celerity") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Celerity") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Celerity") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Celerity") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Celerity") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_celerity_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Blood Sorcery":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Blood Sorcery") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Blood Sorcery") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Blood Sorcery") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Blood Sorcery") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Blood Sorcery") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Blood Sorcery") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_blood_sorcery_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Auspex":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Auspex") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Auspex") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Auspex") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Auspex") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Auspex") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Auspex") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_auspex_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Animalism":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Superior Animalism") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Animalism") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Animalism") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Animalism") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Animalism") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Animalism") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_animalism_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Superior Abombwe":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Superior Abombwe") == 0:
                        height_o = 761
                        width_o = 640
                    elif reversed_disciplines.index("Superior Abombwe") == 1:
                        height_o = 761
                        width_o = 586
                    elif reversed_disciplines.index("Superior Abombwe") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 761
                            width_o = 534
                        else:
                            height_o = 710
                            width_o = 640
                    elif reversed_disciplines.index("Superior Abombwe") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 710
                            width_o = 640
                        else:
                            height_o = 710
                            width_o = 586
                    elif reversed_disciplines.index("Superior Abombwe") == 4:
                        height_o = 710
                        width_o = 586
                    elif reversed_disciplines.index("Superior Abombwe") == 5:
                        height_o = 660
                        width_o = 640
                    image.alpha_composite(self.superior_abombwe_icon.resize((48,48),1), (width_o, height_o))
                elif discipline_name == "Inferior Visceratika":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Visceratika") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Visceratika") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Visceratika") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Visceratika") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Visceratika") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Visceratika") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_visceratika_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Vicissitude":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Vicissitude") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Vicissitude") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Vicissitude") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Vicissitude") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Vicissitude") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Vicissitude") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_vicissitude_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Valeren":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Valeren") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Valeren") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Valeren") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Valeren") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Valeren") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Valeren") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_valeren_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Thaumaturgy":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Thaumaturgy") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Thaumaturgy") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Thaumaturgy") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Thaumaturgy") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Thaumaturgy") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Thaumaturgy") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_thaumaturgy_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Thanatosis":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Thanatosis") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Thanatosis") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Thanatosis") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Thanatosis") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Thanatosis") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Thanatosis") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_thanatosis_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Temporis":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Temporis") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Temporis") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Temporis") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Temporis") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Temporis") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Temporis") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_temporis_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Spiritus":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Spiritus") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Spiritus") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Spiritus") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Spiritus") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Spiritus") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Spiritus") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_spiritus_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Serpentis":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Serpentis") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Serpentis") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Serpentis") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Serpentis") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Serpentis") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Serpentis") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_serpentis_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Sanguinus":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Sanguinus") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Sanguinus") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Sanguinus") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Sanguinus") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Sanguinus") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Sanguinus") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_sanguinus_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Quietus":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Quietus") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Quietus") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Quietus") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Quietus") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Quietus") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Quietus") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_quietus_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Protean":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Protean") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Protean") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Protean") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Protean") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Protean") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Protean") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_protean_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Presence":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Presence") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Presence") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Presence") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Presence") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Presence") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Presence") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_presence_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Potence":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Potence") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Potence") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Potence") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Potence") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Potence") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Potence") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_potence_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Obtenebration":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Obtenebration") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Obtenebration") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Obtenebration") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Obtenebration") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Obtenebration") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Obtenebration") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_obtenebration_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Oblivion":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Oblivion") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Oblivion") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Oblivion") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Oblivion") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Oblivion") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Oblivion") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_oblivion_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Obfuscate":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Obfuscate") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Obfuscate") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Obfuscate") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Obfuscate") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Obfuscate") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Obfuscate") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_obfuscate_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Obeah":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Obeah") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Obeah") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Obeah") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Obeah") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Obeah") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Obeah") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_obeah_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Necromancy":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Necromancy") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Necromancy") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Necromancy") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Necromancy") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Necromancy") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Necromancy") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_necromancy_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Mytherceria":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Mytherceria") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Mytherceria") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Mytherceria") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Mytherceria") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Mytherceria") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Mytherceria") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_mytherceria_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Melopomine":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Melopomine") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Melopomine") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Melopomine") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Melopomine") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Melopomine") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Melopomine") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_melopomine_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Fortitude":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Fortitude") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Fortitude") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Fortitude") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Fortitude") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Fortitude") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Fortitude") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_fortitude_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Dominate":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Dominate") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Dominate") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Dominate") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Dominate") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Dominate") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Dominate") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_dominate_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Dementation":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Dementation") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Dementation") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Dementation") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Dementation") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Dementation") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Dementation") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_dementation_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Daimonion":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Daimonion") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Daimonion") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Daimonion") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Daimonion") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Daimonion") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Daimonion") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_daimonion_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Chimerstry":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Chimerstry") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Chimerstry") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Chimerstry") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Chimerstry") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Chimerstry") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Chimerstry") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_chimerstry_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Celerity":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Celerity") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Celerity") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Celerity") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Celerity") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Celerity") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Celerity") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_celerity_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Blood Sorcery":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Blood Sorcery") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Blood Sorcery") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Blood Sorcery") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Blood Sorcery") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Blood Sorcery") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Blood Sorcery") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_blood_sorcery_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Auspex":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Auspex") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Auspex") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Auspex") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Auspex") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Auspex") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Auspex") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_auspex_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Animalism":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines)) 
                    if reversed_disciplines.index("Inferior Animalism") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Animalism") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Animalism") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Animalism") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Animalism") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Animalism") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_animalism_icon.resize((44,44),1), (width_o, height_o))
                elif discipline_name == "Inferior Abombwe":
                    sorted_disciplines = sorted(discipline_icons)
                    reversed_disciplines = list(reversed(sorted_disciplines))
                    if reversed_disciplines.index("Inferior Abombwe") == 0:
                        height_o = 763
                        width_o = 640
                    elif reversed_disciplines.index("Inferior Abombwe") == 1:
                        height_o = 763
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Abombwe") == 2:
                        if len(discipline_icons) >= 5:
                            height_o = 763
                            width_o = 534
                        else:
                            height_o = 712
                            width_o = 640
                    elif reversed_disciplines.index("Inferior Abombwe") == 3:
                        if len(discipline_icons) >= 5:
                            height_o = 712
                            width_o = 640
                        else:
                            height_o = 712
                            width_o = 586
                    elif reversed_disciplines.index("Inferior Abombwe") == 4:
                        height_o = 712
                        width_o = 586
                    elif reversed_disciplines.index("Inferior Abombwe") == 5:
                        height_o = 662
                        width_o = 640
                    image.alpha_composite(self.inferior_abombwe_icon.resize((44,44),1), (width_o, height_o))
                    

    def drawArtist(self, card):
        if card.variant == "Classic" and card.card_type == "Vampire":
            artist_font = ImageFont.truetype(self.artist_copyright_font_path, 24)
            card.draw.text((152, 998), "Illus: " + card.artist, font=artist_font, fill=(255, 255, 255, 255), stroke_width=1, stroke_fill=(0, 0, 0, 255))
        elif card.variant == "Old" and card.card_type == "Vampire":
            artist_font = ImageFont.truetype(self.artist_copyright_font_path, 24)
            card.draw.text((56, 998), "Illus: " + card.artist, font=artist_font, fill=(255, 255, 255, 255), stroke_width=1, stroke_fill=(0, 0, 0, 255))
        elif card.variant in [ "Classic", "Old"] and card.card_type == "Minion":
            artist_font = ImageFont.truetype(self.artist_copyright_font_path, 24)
            card.draw.text((160, 998), "Illus: " + card.artist, font=artist_font, fill=(255, 255, 255, 255), stroke_width=1, stroke_fill=(0, 0, 0, 255))
        else:
            artist_font = ImageFont.truetype(self.artist_copyright_font_path, 24)
            card.draw.text((140, 990), "Illus: " + card.artist, font=artist_font, fill=(255, 255, 255, 255), stroke_width=2, stroke_fill=(0, 0, 0, 255))

    def drawCopyright(self, card):
        if card.variant in [ "Classic", "Old"]:
            copyright_font = ImageFont.truetype(self.artist_copyright_font_path, 24)
            card.draw.text((434, 998), card.copyright, font=copyright_font, fill=(255, 255, 255, 255), stroke_width=1, stroke_fill=(0, 0, 0, 255))
        else:
            copyright_font = ImageFont.truetype(self.artist_copyright_font_path, 16)
            card.draw.text((490, 995), card.copyright, font=copyright_font, fill=(255, 255, 255, 255), stroke_width=2, stroke_fill=(0, 0, 0, 255))

    def drawSet(self, card):
        set_text = ""
        if card.set_name:
            set_text = " " + card.set_name
            if card.set_number and card.set_max_number:
                set_text += " " + card.set_number + " / " + card.set_max_number

        base_font = 30
        font = 40
        font_size_computed = False
        set_box_width = 500

        while not font_size_computed:
            set_font = ImageFont.truetype(self.set_font_path, font)
            _ , _ , set_text_width, _= card.draw.textbbox((0,0),set_text, font=set_font)

            if set_text_width < set_box_width:
                font_size_computed = True
            else:
                font -= 1

        digits_offset = len(card.block_number) - 1
        block_font = ImageFont.truetype(self.set_font_path, 36 - digits_offset)
        _ , _ , block_number_width, _ = card.draw.textbbox((0,0),card.block_number, font=set_font)


        if card.card_type == "Vampire":
            if card.variant == "Old":
                card.draw.text((56 + digits_offset, 814 + digits_offset), card.block_number, font=block_font,stroke_width=1, stroke_fill=(0, 0, 0, 255))
                card.draw.text((275 - set_text_width, 982 + (base_font - font) / 2), set_text, font=set_font)
            else:
                card.draw.text((152 + digits_offset, 800 + digits_offset), card.block_number, font=block_font,stroke_width=1, stroke_fill=(0, 0, 0, 255))
                card.draw.text((375 - set_text_width, 982 + (base_font - font) / 2), set_text, font=set_font)

    def drawVampire(self, card):
        if card.variant not in ["FullBleed", "Alternate", "Classic", "Old"] and exists("Template/Custom/Vampire/" + card.variant):
            card.image.alpha_composite(Image.open("Template/Custom/Vampire/" + card.variant).resize((692, 990), 1), (30, 30))
            card.image.alpha_composite(self.textbox_frame, (50, 830))
        elif card.variant not in ["FullBleed", "Alternate", "Classic", "Old"] and exists("Template/Custom/Classic Vampire/" + card.variant):
            card.image.alpha_composite(Image.open("Template/Custom/Classic Vampire/" + card.variant).resize((692, 990), 1), (30, 30))
        elif card.variant not in ["FullBleed", "Alternate", "Classic", "Old"] and exists("Template/Custom/Alternate Vampire/" + card.variant):
            card.image.alpha_composite(Image.open("Template/Custom/Alternate Vampire/" + card.variant).resize((692, 990), 1), (30, 30))
        elif card.variant not in ["FullBleed", "Alternate", "Classic", "Old"] and exists("Template/Custom/Old Vampire/" + card.variant):
            card.image.alpha_composite(Image.open("Template/Custom/Old Vampire/" + card.variant).resize((692, 990), 1), (30, 30))
            card.image.alpha_composite(self.textbox_frame, (150, 830))

            # Clan #
       #     self.drawClan(card, 30, 30)
        else:
            # Clan #
            self.drawClan(card, 30, 30)



        # Name #
        self.drawName(card, 52, 50, 696, 58)
        # Capacity #
        if card.variant == "Classic" or exists("Template/Custom/Classic Vampire/" + card.variant):
            self.drawCapacity(card, 44, 677, 936)
        elif card.variant == "Old" or exists("Template/Custom/Old Vampire/" + card.variant):
            self.drawCapacity(card, 50, 662, 913)
        else:
            self.drawCapacity(card, 44, 684, 928)
        # Class icons #
        self.drawDisciplines(card)
        # Advanced icon #
        self.drawAdvanced(card)
        # Background #
        self.drawBackground(card)
        # Flavour #
        offset = 0
        if self.formatFlavour(card):
            offset = self.drawFlavour(card)
        # Effect #
        effect = self.parseEffect(card)
        effect, _ = self.formatEffect(card, effect, offset = offset)
        self.drawEffect(card, effect)
    def drawMinion(self, card):
        if card.variant not in ["FullBleed", "Alternate", "Classic", "Old"] and exists("Template/Custom/Minion/" + card.variant):
            card.image.alpha_composite(Image.open("Template/Custom/Minion/" + card.variant).resize((692, 990), 1), (30, 30))
            card.image.alpha_composite(self.textbox_frame, (50, 830))
        elif card.variant not in ["FullBleed", "Alternate", "Classic", "Old"] and exists("Template/Custom/Classic Minion/" + card.variant):
            card.image.alpha_composite(Image.open("Template/Custom/Classic Minion/" + card.variant).resize((692, 990), 1), (30, 30))
        elif card.variant not in ["FullBleed", "Alternate", "Classic", "Old"] and exists("Template/Custom/Alternate Minion/" + card.variant):
            card.image.alpha_composite(Image.open("Template/Custom/Alternate Minion/" + card.variant).resize((692, 990), 1), (30, 30))
            card.image.alpha_composite(self.textbox_frame, (150, 830))

            # Minion type #
       #     self.drawClan(card, 30, 30)
        else:
            # Minion type #
            self.drawMinionType(card, 30, 30)


        self.drawMinionType(card, 30, 30)
        # Name #
        self.drawName(card, 52, 50, 696, 58)
        # Blood cost #
        self.drawBlood(card, 44, 78, 896)
        # Pool cost#
        self.drawPool(card, 36, 78, 906)
        # Capacity #
        if card.variant == "Classic" or exists("Template/Custom/Classic Vampire/" + card.variant):
            self.drawCapacity(card, 44, 677, 936)
        else:
            self.drawCapacity(card, 44, 684, 928)
        # Clan #
        self.drawClan(card, 30, 30)
        # Class icons #
        self.drawDisciplines(card)

        # Background #
        self.drawBackground(card)
        # Flavour #
        offset = 0
        if self.formatFlavour(card):
            offset = self.drawFlavour(card)
        # Effect #
        effect = self.parseEffect(card)
        effect, _ = self.formatEffect(card, effect, offset = offset)
        self.drawEffect(card, effect)

